#!/bin/bash
pkg_name="lcgdm-dav"

this_script=`readlink -f "$0"`
this_dir=`dirname "$this_script"`
spec_file="${this_dir}/dist/${pkg_name}.spec"

version=`sed -nre 's/^Version:\s+((([0-9]+)\.)*[0-9]+)/\1/p' "${spec_file}"`

echo "Packaging version $version"
mkdir -p /tmp/${pkg_name}/SOURCES/
cd "$this_dir" > /dev/null
tar czf /tmp/${pkg_name}/SOURCES/${pkg_name}-${version}.tar.gz . --exclude=".git" --exclude=".svn" --exclude="*.pyc" --transform="s:\./:${pkg_name}-${version}/:"
cd - > /dev/null

rpmbuild -bs "${spec_file}" \
    --define="_topdir /tmp/${pkg_name}/" \
    --define='_sourcedir %{_topdir}/SOURCES' \
    --define='_builddir %{_topdir}/BUILD' \
    --define='_srcrpmdir %{_topdir}/SRPMS' \
    --define='_rpmdir %{_topdir}/RPMS' > /dev/null

cp /tmp/${pkg_name}/SRPMS/*.rpm .
rm -rf /tmp/${pkg_name}/

