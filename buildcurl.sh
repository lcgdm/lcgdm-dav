# This script assumes that:
# - pwd is the current build directory

# echo "Current dir: `pwd`"
# mkdir -p external/curl/
# cd external/curl
# echo "Current dir: `pwd`"
# ls -l
# cmake3 . -DCMAKE_INSTALL_PREFIX=`pwd`/bogusinstall -DBUILD_CURL_EXE=false -DBUILD_TESTING=false -DBUILD_SHARED_LIBS=false  -DCMAKE_CXX_FLAGS=-fPIC -DCMAKE_C_FLAGS=-fPIC
# make -j2 install
# cd ../../

echo "Current dir: `pwd`"
mkdir -p external/curl/
cd external/curl
echo "Current dir: `pwd`"
ls -l
cmake3 . -DCMAKE_INSTALL_PREFIX=/tmp/curl/bogusinstall -DBUILD_CURL_EXE=false -DBUILD_TESTING=false -DBUILD_SHARED_LIBS=false  -DCMAKE_CXX_FLAGS=-fPIC -DCMAKE_C_FLAGS=-fPIC
make -j2 install
cd ../../

