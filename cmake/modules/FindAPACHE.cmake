#
#  APACHE_FOUND - System has APACHE
#  APACHE_INCLUDE_DIR - The APACHE include directory
#
#  APACHE_LOCATION
#   setting this enables search for apache libraries / headers in this location

#
# Include directories
#
find_path(APACHE_INCLUDE_DIR
          NAMES httpd.h
          HINTS ${APACHE_LOCATION} ${APACHE_LOCATION}/include
          PATHS /usr/include/httpd
                /usr/include/apache2
          DOC "Path to the apache include files"
)


include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set APACHE_FOUND to TRUE if 
# all listed variables are TRUE
find_package_handle_standard_args(APACHE DEFAULT_MSG APACHE_INCLUDE_DIR)
mark_as_advanced(APACHE_INCLUDE_DIR)
