cmake_minimum_required (VERSION 2.6)

cmake_minimum_required (VERSION 2.6)

find_path(BSD_LIBUTIL_INCLUDE_DIR
    NAMES bsd/libutil.h
    HINTS /usr/include
    DOC "libutil.h header"
)
if (BSD_LIBUTIL_INCLUDE_DIR)
    add_definitions("-DHAVE_BSD_LIBUTIL_H")
    set(LIBUTIL_INCLUDE_DIR ${BSD_LIBUTIL_INCLUDE_DIR})
else ()
    find_path(LIBUTIL_INCLUDE_DIR
        NAMES libutil.h
        HINTS /usr/include
        DOC "libutil.h header"
    )
    add_definitions("-DHAVE_LIBUTIL_H")
endif ()


find_library(LIBUTIL_LIBRARIES
    NAMES util
    HINTS /usr/lib /usr/lib64 /lib /lib64
    DOC "libutil.so library"
)
find_library(LIBBSD_LIBRARIES
    NAMES bsd
    HINTS /usr/lib /usr/lib64 /lib /lib64
    DOC "libbsd.so library"
)

include_directories("${LIBUTIL_INCLUDE_DIR}")

add_library(macaroons SHARED
    base64.c
    macaroons.c
    packet.c
    slice.c
    port.c
    v1.c
    v2.c
    varint.c
    explicit_bzero.c
    timingsafe_bcmp.c
    tweetnacl.c
    sha256.c
)
target_link_libraries(macaroons
    ${LIBUTIL_LIBRARIES} ${LIBBSD_LIBRARIES}
)

install (TARGETS macaroons
    LIBRARY       DESTINATION usr/lib${LIB_SUFFIX}
)
