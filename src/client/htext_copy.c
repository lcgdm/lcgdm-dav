/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include <stdlib.h>
#include <string.h>
#include "htext.h"
#include "htext_private.h"

/**
 * Used to parse the output from the server to know the progress
 */
static size_t htext_copy_write_callback(char *buffer, size_t size, size_t nmemb,
        void *pp)
{
    htext_chunk *partial = (htext_chunk*) pp;
    size_t received = size * nmemb;
    size_t bsize, parsed;
    char *next, *prev;
    char *remaining = (char*) partial->extra;

    htext_write_callback(buffer, size, nmemb, pp);

    if (remaining[0] != '\0') {
        size_t r_size = strlen(remaining);
        memcpy(remaining + r_size, buffer, received);
        bsize = received + r_size;
        memcpy(buffer, remaining, bsize);
        remaining[0] = '\0';
    }
    else
        bsize = received;

    buffer[bsize] = '\0'; /* To prevent strchr from going beyond */

    next = buffer;
    parsed = 0;
    while (parsed < bsize) {
        int nfields, index;
        unsigned count;
        time_t timestamp;
        size_t done;

        nfields = sscanf(next, "Perf Marker\n"
                "\tTimestamp: %ld\n"
                "\tStripe Index: %d\n"
                "\tStripe Bytes Transferred: %zu\n"
                "\tTotal Stripe Count: %d\n"
                "End\n", &timestamp, &index, &done, &count);

        if (nfields == 4) {
            prev = next;
            next = strstr(next, "End");
            /* Maybe sscanf got both numbers, but the last line was incomplete!*/
            if (next == NULL ) {
                memcpy(remaining, prev, bsize - parsed);
                remaining[bsize - parsed] = '\0';
                parsed = bsize;
            }
            else {
                if (partial->handle->partials != count) {
                    partial->handle->partial_done = realloc(
                            partial->handle->partial_done,
                            sizeof(size_t) * count);
                    partial->handle->partial_total = realloc(
                            partial->handle->partial_total,
                            sizeof(size_t) * count);
                    partial->handle->partials = count;
                }

                partial->handle->partial_done[index] = done;
                partial->handle->partial_total[index] = 0;

                next += 4;
                parsed = next - buffer;
            }
        }
        else {
            /* If it doesn't follow, it may be a message */
            if (strncasecmp("success", next, 7) == 0) {
                /* Leave things going */
            }
            else if (strncasecmp("aborted", next, 7) == 0) {
                partial->handle->status = HTEXTS_ABORTED;
            }
            else if (strncasecmp("failed", next, 6) == 0) {
                htext_error(partial->handle, next);
            }
            else {
                memcpy(remaining, next, bsize - parsed);
                remaining[bsize - parsed] = '\0';
            }
            parsed = bsize;
        }
    }

    return received;
}

void *htext_copy_method(void *h)
{
    htext_handle *handle = (htext_handle*) h;
    CURL *curl;
    htext_chunk control;
    char buffer[CURL_MAX_WRITE_SIZE], err_buffer[CURL_ERROR_SIZE];
    int nstreams;

    /* Initialize control and buffer */
    htext_partial_init(&control);
    control.handle = handle;
    control.extra = buffer;

    handle->partial_done = NULL;
    handle->partial_total = NULL;
    handle->partials = 0;

    const char* source = GETSTR(handle, HTEXTOP_SOURCEURL);
    const char* destination = GETSTR(handle, HTEXTOP_DESTINATIONURL);

    /* Create and initialize CURL handle */
    curl = curl_easy_init();

    curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "COPY");
    curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, err_buffer);
    curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, htext_header_callback);
    curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, htext_copy_write_callback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &control);
    curl_easy_setopt(curl, CURLOPT_HEADERDATA, &control);
    curl_easy_setopt(curl, CURLOPT_BUFFERSIZE, 64);
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);
    curl_easy_setopt(curl, CURLOPT_USERAGENT, GETSTR(handle, HTEXTOP_CLIENTID));
    curl_easy_setopt(curl, CURLOPT_CAPATH, GETSTR(handle, HTEXTOP_CAPATH));
    curl_easy_setopt(curl, CURLOPT_CAINFO, GETSTR(handle, HTEXTOP_CAFILE));
    curl_easy_setopt(curl, CURLOPT_SSLCERT,
            GETSTR(handle, HTEXTOP_USERCERTIFICATE));
    curl_easy_setopt(curl, CURLOPT_SSLKEY, GETSTR(handle, HTEXTOP_USERPRIVKEY));
    curl_easy_setopt(curl, CURLOPT_SSLKEYPASSWD,
            GETSTR(handle, HTEXTOP_USERPRIVKEYPASS));
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER,
            GETINT(handle, HTEXTOP_VERIFYPEER));
    curl_easy_setopt(curl, CURLOPT_SHARE, handle->curl_share);
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, handle->headers);

    // COPY destination Source: source
    if (GETINT(handle, HTEXTOP_USE_COPY_FROM_SOURCE)) {
        curl_easy_setopt(curl, CURLOPT_URL, destination);
        snprintf(buffer, sizeof(buffer), "Source: %s", source);
        control.headers = curl_slist_append(control.headers, buffer);
    }
    // COPY source Destination: destination
    else {
        curl_easy_setopt(curl, CURLOPT_URL, source);
        snprintf(buffer, sizeof(buffer), "Destination: %s", destination);
        control.headers = curl_slist_append(control.headers, buffer);
    }

    /* Number of streams */
    nstreams = GETINT(handle, HTEXTOP_NUMBEROFSTREAMS);
    if (nstreams > 1) {
        snprintf(buffer, sizeof(buffer), "X-Number-Of-Streams: %d", nstreams);
        control.headers = curl_slist_append(control.headers, buffer);
    }

    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, control.headers);

    /* The buffer is used afterwards, so make sure it is empty! */
    buffer[0] = '\0';

    if (GETINT(handle, HTEXTOP_VERBOSITY)> 1) {
        curl_easy_setopt(curl, CURLOPT_DEBUGFUNCTION, htext_debug_callback);
        curl_easy_setopt(curl, CURLOPT_DEBUGDATA, &control);
        curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);
    }

    htext_log(handle, "Running the remote copy");
    handle->status = HTEXTS_RUNNING;

    if (curl_easy_perform(curl) != CURLE_OK) {
        htext_error(handle, err_buffer);
    }
    else if (control.http_status >= 400 || handle->status == HTEXTS_FAILED) {
        htext_error(handle, NULL );
    }
    else {
        handle->status = HTEXTS_SUCCEEDED;
    }

    handle->http_status = control.http_status;

    /* Clean up */
    htext_partial_clean(&control);
    curl_easy_cleanup(curl);
    return NULL ;
}
