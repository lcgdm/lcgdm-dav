/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gridsite.h>
#include <openssl/err.h>
#include <stdsoap2.h>
#include <sys/stat.h>
#include "htext_private.h"
#include "soapH.h"

/** Get the length of the URL root location.
 * i.e.:
 *  in:  https://host.domain/path?query
 *  out: 19 (https://host.domain)
 * @note 0 is returned on error
 */
static size_t htext_get_len_of_root(const char *url)
{
    const char *colon = strchr(url, ':');
    if (!colon)
        return 0;
    const char *slash = strchr(colon + 3, '/');
    return slash - url;
}

int htext_delegate(htext_chunk *partial, char *url)
{
    htext_handle *handle = partial->handle;
    char *delegation_id = NULL;
    char *reqtxt = NULL, *certtxt = NULL;
    char *keycert = NULL;
    struct soap *soap_get = NULL, *soap_put = NULL;
    struct tns__getNewProxyReqResponse getNewProxyReqResponse;
    char *ucert = NULL, *ukey = NULL, *capath = NULL;
    int lifetime;
    char delegation_endpoint[1024] = { 0 };
    char err_buffer[512] = { 0 };

    /* Need the build the actual delegation endpoint depending
     * on url being absolute, relative, or a full URL
     */
    if (strncmp(url, "http://", 7) == 0) {
        htext_error(handle, "Plain HTTP can not be used for delegation: %s",
                url);
        return -1;
    }
    else if (strncmp(url, "https://", 8) == 0) {
        strncpy(delegation_endpoint, url, sizeof(delegation_endpoint));
        delegation_endpoint[sizeof(delegation_endpoint) - 1] = '\0';
    }
    else if (strncmp(partial->location, "http://", 7) == 0) {
        htext_error(handle,
                "Plain HTTP can not be used for delegation: (%s) %s",
                partial->location, url);
        return -1;
    }
    else if (url[0] == '/') {
        size_t len_of_root_location = htext_get_len_of_root(partial->location);

        if (len_of_root_location == 0) {
            htext_error(handle, "Malformed location received: %s",
                    partial->location);
            return -1;
        }

        strncpy(delegation_endpoint, partial->location, len_of_root_location);
        strncat(delegation_endpoint, url,
                sizeof(delegation_endpoint) - len_of_root_location);
    }
    else {
        char concat = '?';
        if (strchr(partial->location, '?') != NULL)
            concat = '&';

        snprintf(delegation_endpoint, sizeof(delegation_endpoint), "%s%c%s",
                partial->location, concat, url);
    }

    /* Get from the handle */
    ucert = GETSTR(handle, HTEXTOP_USERCERTIFICATE);
    ukey = GETSTR(handle, HTEXTOP_USERPRIVKEY);
    capath = GETSTR(handle, HTEXTOP_CAPATH);
    lifetime = GETINT(handle, HTEXTOP_PROXYLIFE);

    /* Only one is needed if they are the same */
    if (ucert && !ukey)
        ukey = ucert;
    if (!ucert && ukey)
        ucert = ukey;

    /* Cert and key need to be in the same file */
    if (!ucert || !ukey) {
        htext_error(handle, "User certificate and/or key not specified");
        return -1;
    }
    else if (strcmp(ucert, ukey) == 0) {
        keycert = strdup(ucert);
    }
    else {
        FILE *ifp, *ofp;
        int fd, c, mask;

        keycert = strdup("/tmp/.XXXXXX");

        mask = umask(0600);
        fd = mkstemp(keycert);
        umask(mask);

        if (fd < 0) {
            free(keycert);
            htext_error(handle, "Could not create a temporary file (%d)",
                    errno);
            return -1;
        }
        ofp = fdopen(fd, "w");

        ifp = fopen(ukey, "r");
        while ((c = fgetc(ifp)) != EOF)
            fputc(c, ofp);
        fclose(ifp);

        ifp = fopen(ukey, "r");
        while ((c = fgetc(ifp)) != EOF)
            fputc(c, ofp);
        fclose(ifp);

        fclose(ofp);
    }

    /* Initialize SSL */
    ERR_load_crypto_strings();
    OpenSSL_add_all_algorithms();

    /* Request a new delegation ID */
    soap_get = soap_new();

    if (soap_ssl_client_context(soap_get, SOAP_SSL_DEFAULT, keycert, "", NULL,
            capath, NULL ) == 0) {
        soap_call_tns__getNewProxyReq(soap_get, delegation_endpoint,
                "http://www.gridsite.org/namespaces/delegation-1",
                &getNewProxyReqResponse);

        if (soap_get->error == 0) {
            reqtxt = getNewProxyReqResponse.getNewProxyReqReturn->proxyRequest;
            delegation_id =
                    getNewProxyReqResponse.getNewProxyReqReturn->delegationID;

            htext_log(handle, "Got delegation ID %s", delegation_id);

            /* Generate proxy */
            if (GRSTx509MakeProxyCert(&certtxt, stderr, reqtxt, ucert, ukey,
                    lifetime) == GRST_RET_OK) {
                /* Submit the proxy */
                soap_put = soap_new();

                if (soap_ssl_client_context(soap_put, SOAP_SSL_DEFAULT, keycert,
                        "", NULL, capath, NULL ) == 0) {
                    soap_call_tns__putProxy(soap_put, delegation_endpoint,
                            "http://www.gridsite.org/namespaces/delegation-1",
                            delegation_id, certtxt, NULL );
                    if (soap_put->error) {
                        /* Could not PUT */
#ifndef NO_SOAP_SPRINT
                        soap_sprint_fault(soap_put, err_buffer,
                                sizeof(err_buffer));
                        htext_error(handle, err_buffer);
#else
                        soap_print_fault(soap_put, stderr);
                        handle->status = HTEXTS_FAILED;
#endif
                    }
                    else {
                        htext_log(handle, "Delegated credentials");
                    }
                }
                else { /* soap_put ssl error */
#ifndef NO_SOAP_SPRINT
                    soap_sprint_fault(soap_put, err_buffer, sizeof(err_buffer));
                    htext_error(handle, err_buffer);
#else
                    soap_print_fault(soap_put, stderr);
                    handle->status = HTEXTS_FAILED;
#endif
                }

                soap_free(soap_put);
            }
            else {
                htext_error(handle, "Could not generate the proxy");
            }
        }
        else { /* Could not get ID */
#ifndef NO_SOAP_SPRINT
            soap_sprint_fault(soap_get, err_buffer, sizeof(err_buffer));
            htext_error(handle, err_buffer);
#else
            soap_print_fault(soap_get, stderr);
            handle->status = HTEXTS_FAILED;
#endif
        }
    }
    else { /* soap_get ssl error */
#ifndef NO_SOAP_SPRINT
        soap_sprint_fault(soap_get, err_buffer, sizeof(err_buffer));
        htext_error(handle, err_buffer);
#else
        soap_print_fault(soap_put, stderr);
        handle->status = HTEXTS_FAILED;
#endif
    }

    /* Clean soap_get */
    soap_free(soap_get);
    free(keycert);
    free(certtxt);

    /* Return delegation ID */
    if (handle->status == HTEXTS_FAILED) {
        htext_error(handle, "%s", err_buffer);
        return -1;
    }
    else {
        return 0;
    }
}
