/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef WITH_GNUTLS
#include <gnutls/gnutls.h>
#endif

#ifndef WITHOUT_OPENSSL
#include <openssl/crypto.h>
#include <pthread.h>

static pthread_mutex_t *lock_array;
static int n_locks;

void openSSL_lock(int mode, int n, const char *file, int line)
{
    (void) file;
    (void) line;

    if (mode & CRYPTO_LOCK) {
        pthread_mutex_lock(&(lock_array[n]));
    }
    else {
        pthread_mutex_unlock(&(lock_array[n]));
    }
}

#endif

unsigned long openSSL_thread_id(void)
{
    return (unsigned long) pthread_self();
}

/**
 * Initializes locking for OpenSSL/GnuTLS
 * @return 0 on success.
 */
int htext_locking_setup(void)
{
    static int mutexes_set = 0;
#ifdef WITH_GNUTLS
    if (gnutls_global_init() != 0)
    return 1;
#endif
#ifndef WITHOUT_OPENSSL
    /* Initialize mutexes */
    if (!mutexes_set) {
        int i;

        n_locks = CRYPTO_num_locks();

        /* Won't be freed, but that's fine, as it must survive as long as the process does */
        lock_array =
                (pthread_mutex_t*) OPENSSL_malloc(n_locks * sizeof(pthread_mutex_t));

        for (i = 0; i < n_locks; ++i) {
            pthread_mutex_init(&(lock_array[i]), NULL );
        }

        CRYPTO_set_id_callback(openSSL_thread_id);
        CRYPTO_set_locking_callback(openSSL_lock);

        mutexes_set = 1;
    }
#endif
    return 0;
}
