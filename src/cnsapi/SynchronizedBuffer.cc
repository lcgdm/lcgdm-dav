#include "SynchronizedBuffer.h"

#include <iostream>

SynchronizedBuffer::SynchronizedBuffer(): end(false)
{
	pthread_mutex_init(&this->mux, NULL);
	pthread_cond_init(&this->cond, NULL);
}

/* Method to set the end flag */
void SynchronizedBuffer::set_end(void)
{
	this->end = true;
	pthread_cond_signal(&cond);
}

/* Method to add an elem in the synchronized queue */
int SynchronizedBuffer::put(const entry &elem)
{
	pthread_mutex_lock(&mux);
	this->q.push(elem);
	pthread_mutex_unlock(&mux);
	pthread_cond_signal(&cond);	

	return 0;
}

/* Method to pop an elem from the synchronized queue */
entry SynchronizedBuffer::get()
{
	entry return_elem;
	
	while(return_elem.name.empty())
	{
		pthread_mutex_lock(&mux);
		if(!this->q.empty())
		{
			return_elem = this->q.front();
			this->q.pop();
		}else{
			if(this->end)
			{
				pthread_mutex_unlock(&mux);
				return return_elem;
			}
			else
				pthread_cond_wait(&cond, &mux);
		}
		pthread_mutex_unlock(&mux);
	}
	return return_elem;
}
