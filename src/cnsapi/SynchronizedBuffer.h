#ifndef SYNCHRONIZEDBUFFER_H
#define SYNCHRONIZEDBUFFER_H

#include <iostream>
#include <string>
#include <queue>
#include <pthread.h>

struct entry
{
	std::string name;
	std::string uid;
	std::string gid;
	std::string atime;
	std::string ctime;
	std::string mtime;
	std::string fileclass;
	std::string fileid;
	std::string filemode;
	std::string length;
	std::string iscol;
	std::string status;
};

/* This class can either handle a string or a direnstat object */
class SynchronizedBuffer
{
public:
	SynchronizedBuffer();

	int put(const entry &elem);
	entry get();
	void set_end(void);
private:
	std::queue<entry> q;		// Queue handling dir content
	pthread_mutex_t mux;		// Mutex for the queue
	pthread_cond_t cond;		// Condition variable for put signal
	bool end;			// Set to true when parsing finished
};
#endif
