#include "dav_api.h"
#include "dav_private.h"

int Cns_access(const char *path, int amode)
{
  char normalized[CA_MAXPATHLEN];

  /* Basic checks for path validity */
  if (normalize_path(path, normalized, sizeof(normalized)) == NULL )
    return -1;

  /* Check the validity of the amode value */
  if ((amode < 0) || (amode > (R_OK + W_OK + X_OK)))
  {
    errno = EINVAL;
    return -1;
  }

  /* try to open a connection->session if there is no existing one */
  if (!connection)
    if (dav_startsess(NULL, NULL ) != NE_OK)
      return -1;

  struct Cns_filestat statbuf;
  if (Cns_stat(path, &statbuf) != 0)
    return -1;

  mode_t mode = statbuf.filemode;

  /* Check first if R flag has to be checked and
   if the mode own the R permision for the user */
  if ((amode & R_OK) && (!(mode & S_IRUSR)))
  {
    errno = EACCES;
    return -1;
  }

  if ((amode & W_OK) && (!(mode & S_IWUSR)))
  {
    errno = EACCES;
    return -1;
  }

  if ((amode & X_OK) && (!(mode & S_IXUSR)))
  {
    errno = EACCES;
    return -1;
  }

  return 0;
}

