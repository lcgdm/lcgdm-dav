/* DAV backend for Cns API */
#ifndef H_DAV_API
#define H_DAV_API

#include <sys/stat.h>

#define Cns_access    dav_access
#define Cns_chdir     dav_chdir
#define Cns_chmod     dav_chmod
#define Cns_chown     dav_chown
#define Cns_creat     dav_creat
#define Cns_endsess   dav_endsess
#define Cns_getcwd    dav_getcwd
#define Cns_mkdir     dav_mkdir
#define Cns_mkdirg    dav_mkdirg
#define Cns_rmdir     dav_rmdir
#define Cns_startsess dav_startsess
#define Cns_stat      dav_stat
#define Cns_umask     dav_umask
#define Cns_unlink    dav_unlink
#define Cns_utime     dav_utime
#define Cns_opendir   dav_opendir
#define Cns_closedir  dav_closedir
#define Cns_readdir   dav_readdir
#define Cns_readdirx  dav_readdirx

/** DAV specific: Start session allowing to enable or disable SSL
 * @param enable_ssl If set to 0, SSL will be disabled.
 */
int dav_startsessx(char *server, unsigned port, char *comment, int enable_ssl);

/** DAV specific: Do the request using X-Auth headers, so the client
 * can impersonate other users, if trusted.
 */
int dav_startsessx2(char *server, unsigned port, char *comment, int enable_ssl,
                    const char *user_dn, const char **user_fqans,
                    unsigned nfqans);

/** DAV specific: Get an extended attribute (LCGDM::xattr)
 * @param path        The file.
 * @param attrname    The attribute name.
 * @param attrvalue   The value will be stored here.
 * @param valuelength The size of the buffer pointed by attrvalue.
 *                    At exit, length of the value.
 * @return            0 on success.
 */
int dav_attr_get(const char *path, const char *attrname, char *attrvalue,
                 int *valuelength);

/** DAV specific: Set an extended attribute (LCGDM::xattr)
 * @param path      The file.
 * @param attrname  The attribute name.
 * @param attrvalue The value to be stored.
 * @return          0 on success.
 */
int dav_attr_set(const char *path, const char *attrname,
                 const char *attrvalue);

/** DAV specific: Get the specified checksum.
 * Shortcut for dav_attr_get(path, "digest.${checksum_name}", checksum_value,
 *                           &bufsize);
 */
int dav_getchecksum(const char *path, const char *checksum_name,
                    char *checksum_value, int bufsize);

/** DAV specific: Set the specified checksum.
 * Shortcut for dav_attr_set(path, "digest.${checksum_name}", checksum_value);
 */
int dav_setchecksum(const char *path, const char *checksum_name,
                    const char *checksum_value);

/* Not implemented yet, so fallback to dpns */
#define Cns_lstat     dpns_lstat
#define Cns_readdirc  dpns_readdirc
#define Cns_readdirxc dpns_readdirxc

#undef  NSTYPE_LFC
#undef  NSTYPE_DPNS
#define NSTYPE_DAV

#include <stddef.h>
#include <Cns_api.h>
  
#endif
