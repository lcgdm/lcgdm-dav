#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "dav_api.h"
#include "dav_private.h"


static char* dav_attr_fqn(const char *attrname)
{
  char *fqn;
  fqn = malloc(strlen(attrname) + sizeof("LCGDM:") + 1);
  sprintf(fqn, "LCGDM:%s", attrname);
  return fqn;
}



int dav_attr_get(const char *path, const char *attrname, char *attrvalue,
                 int *valuelength)
{
  char normalized[CA_MAXPATHLEN];
  if(normalize_path(path, normalized, sizeof(normalized)) == NULL)
    return -1;

  if(!connection && dav_startsess(NULL, NULL) != NE_OK)
    return -1;

  char *qualified_attrname = dav_attr_fqn(attrname);

  char *value = get_parameter(normalized, qualified_attrname);
  free(qualified_attrname);

  if (!value) {
    *valuelength = 0;
    return -1;
  }

  strncpy(attrvalue, value, *valuelength);
  *valuelength = strlen(attrvalue);
  free(value);

  return 0;
}



int dav_attr_set(const char *path, const char *attrname,
                 const char *attrvalue)
{
  char normalized[CA_MAXPATHLEN];
  if(normalize_path(path, normalized, sizeof(normalized)) == NULL)
    return -1;

  if(!connection && dav_startsess(NULL, NULL) != NE_OK)
    return -1;

  char *qualified_attrname = dav_attr_fqn(attrname);

  int r = set_parameter(normalized, qualified_attrname, attrvalue);
  free(qualified_attrname);

  return r;
}



int dav_getchecksum(const char *path, const char *checksum_name,
                    char *checksum_value, int bufsize)
{
  char *prop_name;
  prop_name = malloc(strlen(checksum_name) + sizeof("checksum.") + 1);

  sprintf(prop_name, "checksum.%s", checksum_name);

  int r = dav_attr_get(path, prop_name, checksum_value, &bufsize);
  checksum_value[bufsize] = '\0';

  free(prop_name);
  return r;
}



int dav_setchecksum(const char *path, const char *checksum_name,
                    const char *checksum_value)
{
  char *prop_name;
  prop_name = malloc(strlen(checksum_name) + sizeof("checksum.") + 1);

  sprintf(prop_name, "checksum.%s", checksum_name);

  int r = dav_attr_set(path, prop_name, checksum_value);
  free(prop_name);
  return r;
}
