#include "dav_api.h"
#include "dav_private.h"

int Cns_chdir(const char *path)
{
	/* Normalized the new path by considering cwd, path and resolving . and .. */
	char normalized[CA_MAXPATHLEN];
  if (normalize_path(path, normalized, sizeof(normalized)) == NULL)
    return -1;

	struct Cns_filestat statbuf;	
	if(dav_stat(path, &statbuf) == -1)
		return -1;

	if(!S_ISDIR(statbuf.filemode)){
		serrno = ENOTDIR;
		return -1;
	}

  /* Save the dav_cwd unnormalized because
    other functions will reapply normalization
    to it. */
	strncpy(dav_cwd, path, sizeof(dav_cwd));

	return 0;	
}
