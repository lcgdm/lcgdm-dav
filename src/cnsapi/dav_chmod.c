#include "dav_api.h"
#include "dav_private.h"

int Cns_chmod(const char *path, mode_t mode)
{
  char normalized[CA_MAXPATHLEN];
  
	/* Basic checks for path validity */
	if(normalize_path(path, normalized, sizeof(normalized)) == NULL)
		return -1;

	/* try to open a session if there is no existing one */
	if(!connection)
		if (dav_startsess(NULL,NULL) != NE_OK)
			return -1;

	char *mode_str = mode_t_to_str(mode);
	char buffer_mode[6];
	strcpy(buffer_mode, "040");;
	strcat(buffer_mode, mode_str);
	free(mode_str);

	if (set_parameter(normalized, "LCGDM:mode", buffer_mode) != NE_OK)
	{
		char *error_code = get_error_code(ne_get_error(connection->session));

		/* Analyze error code */
		if (strcmp(error_code, "403") == 0)
			errno = find403error(normalized);
		else if (strcmp(error_code, "404") == 0)
			errno = ENOENT;
		else if (strcmp(error_code, "405") == 0)
			errno = ENOTDIR;

		return -1;
	}

	return 0;
}
