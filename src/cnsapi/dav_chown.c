#include <limits.h>
#include "dav_api.h"
#include "dav_private.h"

int Cns_chown(const char *path, uid_t new_uid, gid_t new_gid)
{
  char normalized[CA_MAXPATHLEN];

  /* Basic checks for path validity */
  if (normalize_path(path, normalized, sizeof(normalized)) == NULL )
    return -1;

  /* try to open a session if there is no existing one */
  if (!connection)
    if (dav_startsess(NULL, NULL ) != NE_OK)
      return -1;

  /* Checking the uid and gid format
   This number have to be -1 for no modification
   or less than unsigned shot max value
   */
  if ((new_uid < -1) || (new_uid > USHRT_MAX))
  {
    errno = EINVAL;
    return -1;
  }

  if ((new_gid < -1) || (new_gid > USHRT_MAX))
  {
    errno = EINVAL;
    return -1;
  }

  if (new_uid != -1)
  {
    if (set_parameter(normalized, "DAV:group", "111") != NE_OK)
    {
      char *error_code = get_error_code(ne_get_error(connection->session));

      /* Analyze error code */
      if (strcmp(error_code, "403") == 0)
        errno = EACCES;
      else if (strcmp(error_code, "404") == 0)
        errno = ENOENT;
      else if (strcmp(error_code, "405") == 0)
        errno = ENOTDIR;
      return -1;
    }
  }
  return 0;
}
