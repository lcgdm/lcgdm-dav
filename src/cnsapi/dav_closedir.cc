#include "dav_opendir.h"
#include "dav_private.h"

using namespace std;

extern "C" int Cns_closedir(Cns_DIR *dirp)
{
  
	/* Case where dirp is a NULL pointer */
	if(dirp == NULL)
	{
		errno = EFAULT;
		return -1;
	}

	dav_DIR *dir = (dav_DIR *) dirp;


	if(dir->ctx->buf)
		delete dir->ctx->buf;
	if(dir->ctx->d)
		delete dir->ctx->d;
	if(dir->ctx->ds)
		delete dir->ctx->ds;
	if(dir->ctx)
		delete dir->ctx;
	if(dir)
		delete dir;		

	return 0;
}
