#define _GNU_SOURCE
#include <neon/ne_dates.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include "dav_api.h"
#include "dav_private.h"

__thread char dav_cwd[CA_MAXPATHLEN+1] = {0};
__thread int  neon_error;

/* This structure is used to retrieve the value of a property */
typedef struct{
	ne_propname *name;
	char *value;
} property;

/* Debug function */
void debug_msg(const char *fmt, ...)
{
  if (getenv("NS_DAV_DEBUG") != NULL) {
    va_list args;
    va_start(args, fmt);
    fputs("[DEBUG] ", stderr);
    vfprintf(stderr, fmt, args);
    fputc('\n', stderr);
    va_end(args);
  }
}


int *C__serrno(void)
{
	return &errno;
}

/* This callback retrieve the value of the property in property->name and store it in property->value */
void callback_retr_param(void *userdata, const char *href, const ne_prop_result_set *results)
{
	property *prop = (property *) userdata;
	ne_propname *name = (ne_propname *) prop->name;
	
	if(ne_propset_value(results, name))
		prop->value = strdup(ne_propset_value(results, name));
}

time_t convert_timestr_to_timet(char *timestr, int timeformat)
{
	switch(timeformat)
	{
		case RFC2068:
			return ne_rfc1123_parse(timestr);
		case RFC3339:
      return ne_iso8601_parse(timestr);
		default:
			return 0;
	}	
}

/* Dav server return 403 error for both DPNS Errno EACCES and ENOTDIR, 
   this function allows to discriminate the correct error code */
int find403error(const char *path)
{
	int error = EACCES;

	char *iscollection = get_parameter(path, "DAV:iscollection");
	if(iscollection)
	{
		if(atoi(iscollection) == 0)
			return EACCES;
		free(iscollection);
	}

	char *p = strdup(path);
	char *buffer, *elem;

	char *current_path = (char *)malloc(1024 * sizeof(char));
	strcpy(current_path, "/");

	elem = strtok_r(p, "/", &buffer);
	while(elem)
	{
		strcat(current_path, elem);
		strcat(current_path, "/");

		/* Retrieve the parameter iscoloection, put NULL if there is a failure */
		char *iscollection = get_parameter(current_path, "DAV:iscollection");
		if(!iscollection)
			break;	

		if(atoi(iscollection) == 0)
		{
			error = ENOTDIR;
			free(iscollection);
			break;
		}

		free(iscollection);
		elem = strtok_r(NULL, "/", &buffer);
	}
	free(current_path);
	free(p);
	return error;
}

/* Try to retreive the error code from a ne_get_error, 
*  Return NULL otherwise
*/
char *get_error_code(const char *ne_error)
{
	char *buffer, *code; 	//required to use strtok_r


	code = strtok_r((char *)ne_error, " ", &buffer);
	while(code && strlen(code) != 3)
		code = strtok_r(NULL, " ", &buffer);

	return code;
}

/* This function return the value of the parameter, return NULL otherwise*/
char *get_parameter(const char *path, const char *parameter)
{
	/* return NULL if one of the param is NULL*/
	if((!path) || (!parameter))
		return NULL;

	ne_propname names[2];
	char param[1024], nspace[512];

	strcpy(param, parameter);
	char *element, *buffer;

	/* Split the parameter in order to retreive the
	 * namespace and the name (parameter=nspace:name */
	element = strtok_r(param, ":", &buffer);
 	strncpy(nspace, element, 512);
	strcat(nspace, ":");	
	names[0].nspace = (const char *) strdup(nspace);
	
	element = strtok_r(NULL, ":", &buffer);
	names[0].name = (const char *) strdup(element);

	names[1].name = NULL;

	ne_propfind_handler *handler;
	handler = ne_propfind_create(connection->session, path, NE_DEPTH_ZERO);

	property *prop = (property *)malloc(sizeof(property));
	prop->name = names;
	prop->value = NULL;
	
	ne_propfind_named(handler, names, callback_retr_param, (void *)prop);

	char *ret;

	/* Return NULL if the file does not exist, return the value 
 	   of the property otherwise */
	if(prop->value)
		ret = (char *)strdup((char *)prop->value);
	else
		ret = NULL;

	/* Free all structure used for this function */
	free((char *)names[0].nspace);
	free((char *)names[0].name);
	free(prop->value);
	free(prop);
	ne_propfind_destroy(handler);

	return ret;
}

/*
*  This function convert a mode_t parameter into a str one
*/
char *mode_t_to_str(mode_t mode)
{

	char *return_mode = (char *)malloc(sizeof(char) * 16);
	char str_mode[4];
	int size_mode;

	sprintf(str_mode, "%o", mode);
	size_mode = strlen(str_mode);

	memset(str_mode, 0, 4);
	int i = 0;
	for (i=0; i < (3 - size_mode); i++)
		strcat(str_mode, "0");

	sprintf(return_mode, "%s%o", str_mode, mode);

	return return_mode;
}

int no_ssl_verification(void *userdata, int failures, const ne_ssl_certificate *cert)
{
	return 0;
}

/* This function adds the content of dav_cwd to a relative path.
   It is used to enable the chdir functionality.
   */
char *normalize_path(const char * src, char *dst, size_t dst_size)
{
  if (src == NULL) {
    errno = EFAULT;
    return NULL;
  }
  
  size_t res_len;
  size_t src_len = strlen(src);

  const char * ptr = src;
  const char * end = src + src_len;
  const char * next;
  char       * escaped;
    

  if (src[0] != '/') {
    // relative path
    char pwd[PATH_MAX];
    size_t pwd_len;

    if (dav_getcwd(pwd, sizeof(pwd)) == NULL) {
            return NULL;
    }
    pwd_len = strlen(pwd);
    
    if (pwd_len + src_len > CA_MAXPATHLEN) {
      errno = ENAMETOOLONG;
      return NULL;
    }

    strncpy(dst, pwd, dst_size);
    res_len = pwd_len;
  } else {
    if (src_len > CA_MAXPATHLEN) {
      errno = ENAMETOOLONG;
      return NULL;
    }
    res_len = 0;
  }

  for (ptr = src; ptr < end; ptr=next+1) {
    size_t len;
    next = memchr(ptr, '/', end-ptr);
    if (next == NULL) {
            next = end;
    }
    len = next-ptr;
    switch(len) {
      case 2:
        if (ptr[0] == '.' && ptr[1] == '.') {
          const char *slash = memrchr(dst, '/', res_len);
          if (slash != NULL) {
            res_len = slash - dst;
          }
          continue;
        }
        break;
      case 1:
        if (ptr[0] == '.')
                continue;
        break;
      case 0:
        continue;
    }
    dst[res_len++] = '/';
    memcpy(&dst[res_len], ptr, len);
    res_len += len;
  }

  if (res_len == 0) {
    dst[res_len++] = '/';
  }

  dst[res_len] = '\0';
  
  escaped = ne_path_escape(dst);
  strncpy(dst, escaped, dst_size);
  free(escaped);
  
  return dst;
}

int set_parameter(const char *path, const char *parameter, const char *value)
{

	/*Return -1 if a parameters is NULL */
	if ((!path) || (!parameter) || (!value))
		return -1;

	/* Initialisate a new property,
	 * the one we want to modify  */	
	ne_propname names; 

	char param[1024], nspace[512]; 

	/* Keep a local copy of the parameters 
	 * (variable for the strtok function)  */
	strcpy(param, parameter);
	char *element, *buffer;

	/* Split the parameter in order to retreive the
	 * namespace and the name (parameter=nspace:name */
	element = strtok_r(param, ":", &buffer);
 	strncpy(nspace, element, 512);
	strcat(nspace, ":");	
	names.nspace = (const char *) strdup(nspace);
	
	element = strtok_r(NULL, ":", &buffer);
	names.name =  (const char *) strdup(element);

	/* Proppatch operation */
	ne_proppatch_operation ops[2];
	ops[0].name = &names;
	ops[0].type = ne_propset;
	ops[0].value = (const char *) strdup(value);
	
	ops[1].name = NULL;
	

	int ret = 0;
	/* Apply the new value */
	if ( ne_proppatch(connection->session, path, ops) != NE_OK)
		ret = -1;

	/* Free all used variable*/
	free((char *)names.nspace);
	free((char *)names.name);
	free((char *)ops[0].value);


	return ret;
}
