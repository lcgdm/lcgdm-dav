#include <fcntl.h>
#include <unistd.h>
#include "dav_api.h"
#include "dav_private.h"

int Cns_creat(const char *path, mode_t mode)
{
  char normalized[CA_MAXPATHLEN];
  
	/* Basic checks for path validity */
	if(normalize_path(path, normalized, sizeof(normalized)) == NULL)
		return -1;

	/* try to open a connection->session if there is no existing one */
	if(dav_startsess(NULL,NULL) != 0)
  {
    printf("FAIL\n");
		exit(0);
	}

	char *mode_str = mode_t_to_str(mode);
	char buffer_mode[6+1]; // +1 for the NULL terminator
	/* The creation mode is set by the umask if a NULL value is given*/
	if(!mode)
		mode = 0777 & ~internal_umask;
	
	strcpy(buffer_mode, "040");;
	strcat(buffer_mode, mode_str);

	free(mode_str);

	int fd = open("/dev/null", O_RDONLY);

	if (ne_put(connection->session, normalized, fd) != NE_OK)
	{
		char *error_code = get_error_code(ne_get_error(connection->session));

		/* Analyze error code */
		if (strcmp(error_code, "403") == 0)
			errno = find403error(normalized);
		else if (strcmp(error_code, "404") == 0)
			errno = ENOENT;
		else if (strcmp(error_code, "405") == 0)
			errno = EEXIST;
		else if (strcmp(error_code, "409") == 0)
			errno = EISDIR;

		return -1;

	}

	close(fd);

	/* Try to put right permission on the file */
	set_parameter(normalized, "LCGDM:mode", buffer_mode);

	return 0;
}
