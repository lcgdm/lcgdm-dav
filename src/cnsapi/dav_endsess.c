#include "dav_api.h"
#include "dav_private.h"

/* Cns_endsess - Close the connection to the dpns server  
*  ERRORS:
*
*  SENOSHOST    : SENOSHOST, SENOSSERV, SECOMERR, ENSNACT
*/

int Cns_endsess(void)
{
	if(!connection)
	{
		errno = SENOSHOST;
		return -1;
	}

	/* Destroy the neon session and release the connection structure memory */
	if( mutex == 1)
	{
		ne_session_destroy(connection->session);
		free(connection);
		mutex = 0;
	}
	return 0;
}
