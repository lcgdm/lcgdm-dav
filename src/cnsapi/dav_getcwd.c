#include <stdlib.h>
#include "dav_api.h"
#include "dav_private.h"

char *Cns_getcwd(char *buf, int size)
{
	/* If Cns_cwd has not been set or does not exist anymore */
	if(dav_cwd[0] == 0)
	{
		errno = ENOENT;
		return NULL;
	}

	/* buf is NULL and size less than or equal to 0 */
	if((!buf) && (size <= 0))
	{
		errno = EINVAL;
		return NULL;
	}

	/* buf is NULL and size less than size of current working directory + 1 */
	if((!buf) && (size < (strlen(dav_cwd) + 1)))
	{
		errno = ERANGE;
		return NULL;
	}

	
	/* buf is NULL and memory could not be allocated. */
	if ((!buf) && ((buf = (char *)malloc(size * sizeof(char))) == NULL))
	{
		errno = ENOMEM;
		return NULL;
	}

	memset(buf, '\0', (CA_MAXPATHLEN + 1));
	strncpy(buf, dav_cwd, strlen(dav_cwd));

	return buf;
}
