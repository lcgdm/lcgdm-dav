#include "dav_api.h"
#include "dav_private.h"

/* Cns_mkdir - create a new DPNS directory in the name server  
*  ERRORS:
*
*  SENOSHOST    : SENOSHOST, SENOSSERV, SECOMERR, ENSNACT (comes with startsess)
*  EFAULT, ENAMETOOLONG : OK (comes with basic_path_checks)
*  EACCES       : OK (403)
*  ENOENT	: OK (409)
*  EEXIST	: EEXIST, ENOTDIR (405)
* 
*  Missing erros:
*  ENOSPC :The name server database is full.  
*/

/* Mkdir with guid setting */
int Cns_mkdirg(const char *path, const char *guid, mode_t mode)
{
  char normalized[CA_MAXPATHLEN];
  
	/* Basic checks for path validity */
	if(normalize_path(path, normalized, sizeof(normalized)) == NULL)
		return -1;

	/* try to open a connection->session if there is no existing one */
	if(!connection)
		if (dav_startsess(NULL,NULL) != NE_OK)
			return -1;

	char buffer_mode[7]; // last char is fro the NULL terminator
	/* The creation mode is set by the umask if a NULL value is given*/
	if(!mode)
		mode = 0777 & ~internal_umask;
	
	strncpy(buffer_mode, "040", 4);

	char *str_mode = mode_t_to_str(mode);
	strncat(buffer_mode, str_mode, 4);
	free(str_mode);

	/* Try to create the directory */
	if (ne_mkcol(connection->session, normalized) != NE_OK)
	{

		char *error_code = get_error_code(ne_get_error(connection->session));

		/* Analyze error code */
		if (strcmp(error_code, "403") == 0)
			errno = find403error(normalized);
		else if (strcmp(error_code, "405") == 0)
			errno = EEXIST;
		else if (strcmp(error_code, "409") == 0)
			errno = ENOENT;

		return -1;
	}	

	/* Try to put right permission on the file */
	set_parameter(normalized, "LCGDM:mode", buffer_mode);
	
	/* Case where guid has to be added */
	if (guid)
		set_parameter(normalized, "LCGDM:guid", guid);

	return 0;	
}

/* Simple mkdir, no matter of guid */
int Cns_mkdir(const char *path, mode_t mode)
{
	return (dav_mkdirg(path, NULL, mode));
}
