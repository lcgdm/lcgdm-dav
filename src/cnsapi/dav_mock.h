#ifndef H_DAV_MOCK
#define H_DAV_MOCK

#include <utime.h>
/* FAKE */
struct Cns_direncomm {
	char		*comment;
	unsigned short	d_reclen;	/* length of this entry */
	char		d_name[256];
};

struct Cns_direnstatc {
	u_signed64	fileid;
	mode_t		filemode;
	int		nlink;		/* number of files in a directory */
	uid_t		uid;
	gid_t		gid;
	u_signed64	filesize;
	time_t		atime;		/* last access to file */
	time_t		mtime;		/* last file modification */
	time_t		ctime;		/* last metadata modification */
	short		fileclass;	/* 1 --> experiment, 2 --> user */
	char		status;		/* ' ' --> online, 'm' --> migrated */
	char		*comment;
	unsigned short	d_reclen;	/* length of this entry */
	char		d_name[256];
};

struct Cns_direnstatg {
	u_signed64	fileid;
	char		guid[CA_MAXGUIDLEN+1];
	mode_t		filemode;
	int		nlink;		/* number of files in a directory */
	uid_t		uid;
	gid_t		gid;
	u_signed64	filesize;
	time_t		atime;		/* last access to file */
	time_t		mtime;		/* last file modification */
	time_t		ctime;		/* last metadata modification */
	short		fileclass;	/* 1 --> experiment, 2 --> user */
	char		status;		/* ' ' --> online, 'm' --> migrated */
	char		csumtype[3];
	char		csumvalue[33];
	unsigned short	d_reclen;	/* length of this entry */
	char		d_name[256];
};

struct Cns_list {
        int             fd;             /* socket for communication with server */
        int             eol;            /* end of list */
        int             offset;         /* offset in buffer */
        int             len;            /* amount of data in buffer */
        char            *buf;           /* cache buffer for list entries */
};
typedef struct Cns_list Cns_list;

struct Cns_filereplica {
        u_signed64      fileid;
        u_signed64      nbaccesses;
        time_t          atime;          /* last access to replica */
        time_t          ptime;          /* replica pin time */
        char            status;
        char            f_type;
        char            poolname[CA_MAXPOOLNAMELEN+1];
        char            host[CA_MAXHOSTNAMELEN+1];
        char            fs[80];
        char            sfn[CA_MAXSFNLEN+1];
};


/* Fake function, not yet implemented */
int dav_getcomment(const char *path, char *comment);
int dav_readlink(const char *, char *, size_t);
#endif
