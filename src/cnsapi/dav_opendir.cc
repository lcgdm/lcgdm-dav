#include "dav_opendir.h"
#include "dav_private.h"

using namespace std;

/* Opendir method */
extern "C" Cns_DIR *Cns_opendir(const char *path)
{
  char normalized[CA_MAXPATHLEN];
  
  if(normalize_path(path, normalized, sizeof(normalized)) == NULL)
		return NULL;

	if(!getenv("DPNS_HOST")){
		printf("DPNS_HOST has to bet set\n");
		return NULL;
	}

	struct Cns_filestat statbuf;
	if(dav_stat(path, &statbuf) == -1)
		return NULL;

	dav_DIR *dir = new dav_DIR();
	dir->dirname = normalized;

	dir->ctx = new request_ctx();
	dir->ctx->req = NULL;
	dir->ctx->sess = connection->session;
	dir->ctx->buf = new SynchronizedBuffer();
	return (Cns_DIR*)dir;
}	
