#ifndef H_DAV_OPENDIR
#define H_DAV_OPENDIR

#include <dirent.h>
#include <iostream>
#include <neon/ne_request.h>
#include <string>
#include <queue>
#include "dav_api.h"
#include "SynchronizedBuffer.h"

/* Request type */
#define EMPTY_REQUEST 	0
#define FULL_REQUEST 	1

#ifdef __cplusplus
extern "C" {
#endif 

/* Structure used to provide the buffer to the parsing thread */
struct request_ctx{
	ne_request 		*req;
	ne_session 		*sess;
	SynchronizedBuffer 	*buf;
	entry			    *current_entry;
	dirent			  *d;
	Cns_direnstat	*ds;
	int			       type;
};

/* Structure returned by opendir */
typedef struct{
	std::string	dirname;
	request_ctx	*ctx;
} dav_DIR;


/* XML Parsing thread */
void 	*parse_xml(void *params);

/* Sax handler when a XML tag is found */
int 	ne_elem_start(void *, int, const char *, const char *, const char **);
int 	ne_elem_data(void *userdata, int state, const char *cdata, size_t len);
int 	ne_elem_end(void *, int, const char *, const char *);

/* XML reader callback */
int reader_callback(void *, const char *, size_t);
#ifdef __cplusplus
}
#endif 
#endif
