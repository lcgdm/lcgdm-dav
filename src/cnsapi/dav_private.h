#ifndef H_DAV_STRUCT
#define H_DAV_STRUCT

#include <errno.h>
#include <fcntl.h>
#include <neon/ne_basic.h>
#include <neon/ne_props.h>
#include <neon/ne_request.h>
#include <serrno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

/* Variable for the RFC maks (using in date conversion) */
#define RFC2068 0
#define RFC3339 1

/**/
#define HREF		5
#define PROP		10

/* Dirent property */
#define PROP_NAME 	11
#define PROP_UID 	13
#define PROP_GID 	14
#define PROP_ATIME 	15
#define PROP_CTIME 	16
#define PROP_MTIME 	17
#define PROP_FILECLASS 	18
#define PROP_FILEID 	19
#define PROP_FILEMODE 	20
#define PROP_LENGTH 	21
#define PROP_ISCOL 	22
#define PROP_STATUS 	23


struct dav_connection{
	char 		server[CA_MAXHOSTNAMELEN+1];
	ne_session 	*session;
	char        *user_dn;
	char        **fqans;
	unsigned     nfqans;
};


/* Thread local variable */
extern __thread struct dav_connection	*connection;
extern __thread char			dav_cwd[CA_MAXPATHLEN+1];
extern __thread int 			neon_error; 
extern __thread mode_t 			internal_umask;
extern __thread int			mutex;

extern pthread_once_t 	init_once;

#ifdef __cplusplus
extern "C" {
#endif

/* Debug function */
void debug_msg(const char *fmt, ...);

/* This callback method is used to retrieve the value of the property in 
   property->name and store it in property->value */
void 	callback_retr_param(void *userdata, const char *href, const ne_prop_result_set *results);

/* This function convert a string formated date to a time_t timestamp */
time_t	convert_timestr_to_timet(char *timestr, int timeformat);

/* 403 errot can have multiple meaning: EACCES or ENOTDIR 
   This function returns the correct meaning of the 403 by checking 
   reccursivelly each elem of path. If a file is found in the path,
   ENOTDIR is returned, EACCES otherwise */
int 	find403error(const char *path);

/* Standard neon error message are like <error code : error message>.
   In order for this message to be compare easily, this function
   returns only the error code from the message */
char 	*get_error_code(const char *ne_error);

/*This function return the value of the webdav <property>
  of the <path>, return NULL if there is a failure */
char 	*get_parameter(const char *path, const char *property);

/* Convert a mode_t to a string */
char 	*mode_t_to_str(mode_t mode);

/* This callback used by neon always returns 0*/
int 	no_ssl_verification(void *, int, const ne_ssl_certificate *);

/* Given a path and considering the current working directory, return the normalized path */
char 	*normalize_path(const char * src, char *dst, size_t dst_size);

/* This function sets a webdav property. The <value> is set to the <property>
   of the <path> */
int 	set_parameter(const char *path, const char *property, const char *value);

/* pthread init */
void 	thread_init_once(void);

/* Name server call */
int	dav_access(const char *path, int amode);

#ifdef __cplusplus
};
#endif

#endif
