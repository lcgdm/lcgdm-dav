#include "dav_opendir.h"
#include "dav_private.h"
#include <neon/ne_utils.h>

using namespace std;


/* Callback when a xml tag is found */
int ne_elem_start(void *userdata, int parent,
                   const char *nspace, const char *name,
                   const char **atts)
{
	request_ctx *ctx = (request_ctx *)userdata;

	/* DETECT PROPERTY */
	if ((ctx->type == FULL_REQUEST) && (strcmp(name, "prop") == 0))
	{
		if(ctx->current_entry != NULL)
			printf("FATAL ERROR\n");
		else
			ctx->current_entry = new entry();
		return PROP;
	}
	if ((ctx->type == EMPTY_REQUEST) && (strcmp(name, "href") == 0))
	{
		if(ctx->current_entry != NULL)
			printf("FATAL ERROR\n");
		else
			ctx->current_entry = new entry();
		return HREF;
	}

	if(ctx->type == FULL_REQUEST)
	{
    if (strcmp(nspace, "DAV:") == 0) {
      if (strcmp(name, "displayname") == 0)
        return PROP_NAME;
      else if (strcmp(name, "owner") == 0)
        return PROP_UID;
      else if (strcmp(name, "group") == 0)
        return PROP_GID;
      else if (strcmp(name, "lastaccessed") == 0)
        return PROP_ATIME;
      else if (strcmp(name, "creationdate") == 0)
        return PROP_CTIME;
      else if (strcmp(name, "getlastmodified") == 0)
        return PROP_MTIME;
      else if (strcmp(name, "getcontentlength") == 0)
        return PROP_LENGTH;
      else if (strcmp(name, "iscollection") == 0)
        return PROP_ISCOL;
    }
    else if (strcmp(nspace, "LCGDM:") == 0) {
      if (strcmp(name, "fileclass") == 0)
        return PROP_FILECLASS;
      else if (strcmp(name, "fileid") == 0)
        return PROP_FILEID;
      else if (strcmp(name, "mode") == 0)
        return PROP_FILEMODE;
      else if (strcmp(name, "status") == 0)
        return PROP_STATUS;
    }
	}
	return 1;
}

/* Callback containing the content of a xml tag */
int ne_elem_data(void *userdata, int state, const char *cdata, size_t len)
{
	request_ctx *ctx = (request_ctx *)userdata;
  char        *aux;
	
	string buffer (cdata, len);
	switch(state){
		case PROP_NAME:
      /* Entities (as &amp;) trigger extra calls being already escaped */
      ctx->current_entry->name += buffer;
			break;
		case PROP_UID:
			ctx->current_entry->uid = buffer;
			break;
		case PROP_GID:
			ctx->current_entry->gid = buffer;
			break;
		case PROP_ATIME:
			ctx->current_entry->atime = buffer;
			break;
		case PROP_CTIME:
			ctx->current_entry->ctime = buffer;
			break;
		case PROP_MTIME:
			ctx->current_entry->mtime = buffer;
			break;
		case PROP_FILECLASS:
			ctx->current_entry->fileclass = buffer;
			break;
		case PROP_FILEID:
			ctx->current_entry->fileid = buffer;
			break;
		case PROP_FILEMODE:
			ctx->current_entry->filemode = buffer;
			break;
		case PROP_LENGTH:
			ctx->current_entry->length = buffer;
			break;
		case PROP_ISCOL:
			ctx->current_entry->iscol = buffer;
			break;
		case PROP_STATUS:
			ctx->current_entry->status = buffer;
			break;
}

	/* Put the current entry in the synchronized buffer */
	if (state == HREF) {

		if (buffer.length() == (buffer.find_last_of('/') + 1))
			buffer = buffer.substr(0, buffer.length() - 1);

		buffer = buffer.substr(buffer.find_last_of('/') + 1, buffer.length());
	
    aux = ne_path_unescape(buffer.c_str());
    if (aux) {
      ctx->current_entry->name = aux;
      free(aux);
    }
    else {
      ctx->current_entry->name = buffer + "(!)";
    }		
	}
	return 0;

}

int ne_elem_end(void *userdata, int state, const char *nspace, const char *name)
{
	request_ctx *ctx = (request_ctx *)userdata;

	if (	((state == HREF) && (ctx->type == EMPTY_REQUEST))
		|| 
		((state == PROP) && (ctx->type == FULL_REQUEST))
    )
	{
		ctx->buf->put(*(ctx->current_entry));

		delete ctx->current_entry;
		ctx->current_entry = NULL ;
  }	
	
	return 0;
}

int reader_callback(void *userdata, const char *buf, size_t len) {
	ne_xml_parser *parser = (ne_xml_parser *)userdata;
	ne_xml_parse(parser, buf, len);

	return 0;
}

void *parse_xml(void *params)
{
	request_ctx *ctx = (request_ctx *)params;
	ne_request *req = (ne_request *)ctx->req;

	/* Create the parser and set the correct SAX callback*/
	ne_xml_parser *parser;
	parser = ne_xml_create();
	ne_xml_push_handler(parser, ne_elem_start, ne_elem_data, ne_elem_end, (void *)ctx);

	/* Set the XML reader */ 
	ne_add_response_body_reader(req, ne_accept_always, reader_callback, (void *)parser);

	/* Execute the request (PROPFIND) */
	ne_request_dispatch(req);

	/* Return error to the parent thread (errno is thread local) */
//	ctx->error = std::string(ne_get_error(session));

//	/* Destroy the request and set the end flag */	
	ne_request_destroy(req);
	ctx->buf->set_end();
	pthread_exit(0);
}


ne_request *create_request(const char *path, const char *body)
{
	ne_request *req;
	req = ne_request_create(connection->session, "PROPFIND", path);
	ne_add_request_header(req, "Depth", "1");
	ne_set_request_body_buffer(req, body, strlen(body));
	return req;
}

extern "C" struct dirent *Cns_readdir(Cns_DIR *dirp)
{
	/* EFAULT Trigger if dirp is a NULL pointer*/
	if(!dirp)
	{
		serrno = EFAULT;
		return NULL;
	}

	/* Dav empty request */
	static const std::string body("<propfind xmlns=\"DAV:\"><prop></prop></propfind>");

	dav_DIR *dir = (dav_DIR *)dirp;

	/* The request is instanciate only once (during the first read) */
	if (dir->ctx->req == NULL)
	{
		dir->ctx->req = create_request(dir->dirname.c_str(), body.c_str());
		dir->ctx->type = EMPTY_REQUEST; 
		pthread_t parsing_thread;
		pthread_create(&parsing_thread, NULL, parse_xml, (void *)dir->ctx);
		dir->ctx->buf->get();
	}

	/* A dirent structure is instanciate or memseted */
	if(!dir->ctx->d)
		dir->ctx->d = new dirent();
	else
		memset(dir->ctx->d, 0, sizeof(struct dirent));

	/* Retrieve the current element */
	entry elem = dir->ctx->buf->get();

	/* Set structure fields to correct value */
	strncpy(dir->ctx->d->d_name, elem.name.c_str(), 256);
	dir->ctx->d->d_reclen = elem.name.length();

	/* Return the dirent structure */
	if(dir->ctx->d->d_reclen == 0)
		return NULL;

	return dir->ctx->d;
}

extern "C" struct Cns_direnstat *Cns_readdirx(Cns_DIR *dirp)
{
	/* EFAULT Trigger if dirp is a NULL pointer*/
	if(!dirp)
	{
		serrno = EFAULT;
		return NULL;
	}

	/* Dav request asking for every fields */
	static const std::string body("<propfind xmlns=\"DAV:\">\
					<prop>\
					 <displayname xmlns=\"DAV:\" />\
					 <getcontentlength xmlns=\"DAV:\" />\
					 <iscollection xmlns=\"DAV:\" />\
					 <owner xmlns=\"DAV:\" />\
					 <group xmlns=\"DAV:\" />\
					 <fileclass xmlns=\"LCGDM:\"/>\
					 <fileid xmlns=\"LCGDM:\"/>\
					 <mode xmlns=\"LCGDM:\"/>\
					 <status xmlns=\"LCGDM:\"/>\
					 <creationdate xmlns=\"DAV:\" />\
 					 <getlastmodified xmlns=\"DAV:\" />\
					 <lastaccessed xmlns=\"LCGDM:\"/>\
					</prop>\
				       </propfind>");

	dav_DIR *dir = (dav_DIR *)dirp;

	/* The request is instanciate only once (during the first read) */
	if (dir->ctx->req == NULL)
	{
		dir->ctx->req = create_request(dir->dirname.c_str(), body.c_str());
		dir->ctx->type = FULL_REQUEST;
		dir->ctx->current_entry = NULL; 
		pthread_t parsing_thread;
		pthread_create(&parsing_thread, NULL, parse_xml, (void *)dir->ctx);
		dir->ctx->buf->get();
	}

	/* Retreive the current element */
	entry elem = dir->ctx->buf->get();

	/* A dirent structure is instanciate or memseted */
	if(!dir->ctx->ds)
		dir->ctx->ds = new Cns_direnstat();
	else
		memset(dir->ctx->ds, 0, sizeof(struct Cns_direnstat));

	/* Set entry name and length */
	strncpy(dir->ctx->ds->d_name, elem.name.c_str(), 256);
	dir->ctx->ds->d_reclen = elem.name.length();
	
	/* Set Uid and Gid */
	dir->ctx->ds->uid = atoi(elem.uid.c_str());
	dir->ctx->ds->gid = atoi(elem.gid.c_str());

	/* Timestamp */
	dir->ctx->ds->atime = convert_timestr_to_timet((char *)elem.atime.c_str(), RFC2068);
	dir->ctx->ds->mtime = convert_timestr_to_timet((char *)elem.mtime.c_str(), RFC2068);
	dir->ctx->ds->ctime = convert_timestr_to_timet((char *)elem.ctime.c_str(), RFC3339);

	/* Size, nlinks */
	if(atoi(elem.iscol.c_str()) == 1)
	{
		dir->ctx->ds->nlink = atoi(elem.length.c_str());
		dir->ctx->ds->filemode = S_IFDIR;
	}
	else
	{
		dir->ctx->ds->filesize = atoi(elem.length.c_str());
		dir->ctx->ds->filemode = S_IFREG;
    dir->ctx->ds->nlink = 1;
	}

	/* File informations */
	dir->ctx->ds->fileid = atol(elem.fileid.c_str());
	dir->ctx->ds->fileclass = atoi(elem.fileclass.c_str());

	dir->ctx->ds->filemode |= (unsigned)strtol(elem.filemode.c_str(), NULL, 0);
  
  dir->ctx->ds->status = elem.status[0];

	/* Return the direnstat structure */
	if(dir->ctx->ds->d_reclen == 0)
		return NULL;

	return dir->ctx->ds;
}
