/* dpns_startsess - start a neon session
 *  parameter comment is unused right now, it is kept for dpns api compatibility
 *
 *  ERROR:
 *  If the function can not open a session, the Cns_error is set to SENOSHOST
 *  SENOSHOST = SENOSHOST, SENOSSERV, SECOMERR, ENSNACT
 *  EINVAL = OK (the lengh of the comment exceed the max value
 */
#include "../shared/config.h"
#ifdef USE_GNUTLS
#include <gnutls/pkcs12.h>
#else
#include <openssl/err.h>
#include <openssl/pem.h>
#include <openssl/pkcs12.h>
#include <openssl/x509.h>
#endif
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include "dav_api.h"
#include "dav_private.h"

/* Prototypes */
int convert_x509_to_p12(const char *privkey, const char *clicert,
    const char *p12cert);

/* The neon session is thread local */
__thread struct dav_connection *connection = NULL;
__thread int mutex = 0;

pthread_once_t init_once = PTHREAD_ONCE_INIT;

/* Function called when the thread is first called (creation time) */
void thread_init_once(void)
{
  (void) ne_sock_init();
#ifdef USE_GNUTLS
  (void) gnutls_global_init();
#endif
}



void dav_set_headers(ne_request *req, void *userdata,
                     const char *method, const char *requri)
{
  struct dav_connection* connection = (struct dav_connection*)userdata;

  if (connection->user_dn)
    ne_add_request_header(req, "X-Auth-Dn", connection->user_dn);

  if(connection->nfqans) {
    unsigned i;
    char header[64];
    for (i = 0; i < connection->nfqans; ++i) {
      snprintf(header, sizeof(header), "X-Auth-Fqan%u", i);
      ne_add_request_header(req, header, connection->fqans[i]);
    }
  }
}



int dav_startsess_internal(char *server, unsigned port,
                           char *comment, int enable_ssl,
                           const char *user_dn, const char **user_fqans, unsigned nfqans)
{
  FILE *p12 = NULL;
  const char *p12cert = "/tmp/usercert.p12";
  const char *userkey, *usercert, *userproxy;
  char buffer[128];

  /* Default port */
  if (!port)
    port = enable_ssl?443:80;

  /* Function to be executed once per thread, used to create the connection structure and set the server name */
  if (mutex == 0)
  {
    /* If no host specified, use the DPNS default one */
    if (!server)
      server = getenv("DPNS_HOST");

    /* Finish the function if the host is still NULL*/
    if (!server)
    {
      errno = SENOSHOST;
      return -1;
    }

    /* Trigger an error if the comment is too long */
    if (comment && (strlen(comment) > CA_MAXCOMMENTLEN))
    {
      errno = EINVAL;
      return -1;
    }

    pthread_once(&init_once, thread_init_once);

    connection = (struct dav_connection *) calloc(sizeof(struct dav_connection),
        1);
    strcpy(connection->server, server);
    mutex = 1;
  }

  /* exit function if a session already exists */
  if (connection->session)
    return 0;

  /* Retrieve userkey and usercert from environement variable */
  userkey = getenv("X509_USER_KEY");
  usercert = getenv("X509_USER_CERT");
  userproxy = getenv("X509_USER_PROXY");

  /* Use a proxy */
  if (enable_ssl)
  {
    if (userproxy)
    {
      userkey = usercert = userproxy;
    }
    /* Try default proxy location */
    else if (!userkey && !usercert)
    {
      struct stat stat_buf;

      snprintf(buffer, sizeof(buffer), "/tmp/x509up_u%d", getuid());
      /* No luck, try with host cert and key */
      if (stat(buffer, &stat_buf) != 0)
      {
        usercert = "/etc/grid-security/hostcert.pem";
        userkey = "/etc/grid-security/hostkey.pem";
      }
    }

    debug_msg("User certificate: %s", usercert);
    debug_msg("User key:         %s", userkey);

    /* Try to open the certificate, create one if file does not exist yet */
    if ((p12 = fopen(p12cert, "r")) == NULL )
    {
      if (convert_x509_to_p12(userkey, usercert, p12cert) == -1)
      {
        fprintf(stderr, "An error occur in the certificate conversion\n");
        serrno = EPROTO;
        return -1;
      }
    }
    else
    {
      fclose(p12);
    }

    /* Try to open a session, return -1 and set the correct errno if it failed */
    if ((connection->session = ne_session_create("https", server, port))
        == NULL )
    {
      errno = ENSNACT;
      return -1;
    }
  }
  else
  {
    if ((connection->session = ne_session_create("http", server, port)) == NULL )
    {
      errno = ENSNACT;
      return -1;
    }
  }

  /* manual checking for ssl credentials */
  ne_ssl_set_verify(connection->session, no_ssl_verification, NULL );

  /* Read the pkcs12 certificate */
  if (enable_ssl)
  {
    ne_ssl_client_cert *cert = ne_ssl_clicert_read(p12cert);
    if (cert == NULL )
    {
      ne_session_destroy(connection->session);
      errno = SECOMERR;
      return -1;
    }
    ne_ssl_set_clicert(connection->session, cert);
    ne_ssl_clicert_free(cert);
  }

  /* The client may be trying to impersonate someone */
  if (user_dn)
    connection->user_dn = strdup(user_dn);

  if (user_fqans && nfqans) {
    size_t i;

    connection->nfqans = nfqans;
    connection->fqans = calloc(sizeof(char*), nfqans);
    for (i = 0; i < nfqans; ++i)
      connection->fqans[i] = strdup(user_fqans[i]);
  }

  if (connection->user_dn || connection->nfqans)
    ne_hook_create_request(connection->session, dav_set_headers, connection);

  return 0;
}



int dav_startsessx2(char *server, unsigned port, char *comment, int enable_ssl,
                    const char *user_dn, const char **user_fqans,
                    unsigned nfqans)
{
  return dav_startsess_internal(server, port, comment, enable_ssl,
                                user_dn, user_fqans, nfqans);
}



int dav_startsessx(char *server, unsigned port, char *comment, int enable_ssl)
{
  return dav_startsess_internal(server, port, comment, enable_ssl, NULL, NULL, 0);
}



int Cns_startsess(char *server, char *comment)
{
  return dav_startsess_internal(server, 0, comment, 1, NULL, NULL, 0);
}



/*
 This function take a user certificate and a private key in x509 format and
 convert it into pkcs12 format. This function returns -1 if a problem occurs, 0 otherwise
 */
#ifdef USE_GNUTLS
int convert_x509_to_p12(const char *privkey, const char *clicert, const char *p12cert)
{
  gnutls_datum_t data, keyid;
  struct stat fstat;
  FILE *fhandler;
  gnutls_x509_crt_t cert;
  gnutls_x509_privkey_t key;
  gnutls_pkcs12_t p12;
  gnutls_pkcs12_bag_t certbag, keybag;
  int r, index;
  unsigned char buffer[10 * 1024];
  size_t buffer_size;

  /* Init P12 */
  gnutls_pkcs12_init(&p12);

  /* Read the private key file */
  if (stat(privkey, &fstat) != 0)
  {
    printf("Can not stat %s (%s)", privkey, strerror(errno));
    return -1;
  }
  data.size = fstat.st_size;
  data.data = buffer;

  fhandler = fopen(privkey, "rb");
  if (!fhandler)
  {
    printf("Can not open %s (%s)\n", privkey, strerror(errno));
    return -1;
  }

  fread(data.data, sizeof(unsigned char), data.size, fhandler);

  gnutls_x509_privkey_init(&key);
  r = gnutls_x509_privkey_import(key, &data, GNUTLS_X509_FMT_PEM);
  if (r < 0)
  {
    printf("Can not import the private key (%s)\n", gnutls_strerror(r));
    return -1;
  }

  buffer_size = sizeof(buffer);
  gnutls_x509_privkey_get_key_id (key, 0, buffer, &buffer_size);

  keyid.size = buffer_size;
  keyid.data = malloc(sizeof(unsigned char) * keyid.size);
  memcpy(keyid.data, buffer, keyid.size);

  index = gnutls_pkcs12_bag_init(&keybag);
  gnutls_pkcs12_bag_set_key_id(keybag, index, &keyid);

  buffer_size = sizeof(buffer);
  gnutls_x509_privkey_export_pkcs8 (key, GNUTLS_X509_FMT_DER,
      "", GNUTLS_PKCS8_USE_PKCS12_3DES,
      buffer, &buffer_size);
  data.size = buffer_size;
  gnutls_pkcs12_bag_set_data(keybag, GNUTLS_BAG_PKCS8_ENCRYPTED_KEY, &data);

  fclose(fhandler);

  /* Read the user certificate */
  if (stat(clicert, &fstat) != 0)
  {
    printf("Can not stat %s (%s)\n", clicert, strerror(errno));
    return -1;
  }
  data.size = fstat.st_size;

  fhandler = fopen(clicert, "rb");
  if (!fhandler)
  {
    printf("Can not open %s (%s)\n", clicert, strerror(errno));
    return -1;
  }

  fread(data.data, sizeof(unsigned char), data.size, fhandler);

  gnutls_x509_crt_init(&cert);
  r = gnutls_x509_crt_import(cert, &data, GNUTLS_X509_FMT_PEM);
  if (r < 0)
  {
    printf("Can not import the certificate (%s)\n", gnutls_strerror(r));
    return -1;
  }

  index = gnutls_pkcs12_bag_init(&certbag);
  gnutls_pkcs12_bag_set_crt(certbag, cert);
  gnutls_pkcs12_bag_set_key_id(certbag, index, &keyid);

  fclose(fhandler);

  /* Generate P12 */
  gnutls_pkcs12_init(&p12);
  gnutls_pkcs12_set_bag(p12, certbag);
  gnutls_pkcs12_set_bag(p12, keybag);
  gnutls_pkcs12_generate_mac(p12, "");

  buffer_size = sizeof(buffer);
  r = gnutls_pkcs12_export (p12, GNUTLS_X509_FMT_DER, buffer, &buffer_size);

  if (r < 0)
  {
    printf("Can not export the pkcs12 certificate (%s)\n", gnutls_strerror(r));
    return -1;
  }

  fhandler = fopen(p12cert, "wb");
  if (!fhandler)
  {
    printf("Can not open the output file %s (%s)\n", p12cert, strerror(errno));
    return -1;
  }

  fwrite(buffer, buffer_size, 1, fhandler);

  fclose(fhandler);

  /* Release */
  gnutls_pkcs12_deinit(p12);
  gnutls_pkcs12_bag_deinit(certbag);
  gnutls_pkcs12_bag_deinit(keybag);
  gnutls_x509_crt_deinit(cert);
  free(keyid.data);

  return 0;
}
#else
int convert_x509_to_p12(const char *privkey, const char *clicert,
    const char *p12cert)
{
  X509 *cert;
  PKCS12 *p12;
  EVP_PKEY *cert_privkey;
  FILE *certfile, *keyfile, *p12file;
  int bytes = 0;

  OpenSSL_add_all_algorithms();
  ERR_load_crypto_strings();

  /* Read the private key file */
  if ((cert_privkey = EVP_PKEY_new()) == NULL )
  {
    printf("Error creating EVP_PKEY structure.\n");
    return -1;
  }
  if (!(keyfile = fopen(privkey, "r")))
  {
    printf("Error cant read certificate private key file.\n");
    return -1;
  }
  if (!(cert_privkey = PEM_read_PrivateKey(keyfile, NULL, NULL, NULL )))
  {
    printf("Error loading certificate private key content.\n");
    return -1;
  }
  fclose(keyfile);

  /* Read the user certificate */
  if (!(certfile = fopen(clicert, "r")))
  {
    printf("Error cant read certificate file.\n");
    return -1;
  }
  if (!(cert = PEM_read_X509(certfile, NULL, NULL, NULL )))
  {
    printf("Error loading cert into memory.\n");
    return -1;
  }
  fclose(certfile);

  /* Generate the p12 certificate */
  if ((p12 = PKCS12_new()) == NULL )
  {
    printf("Error creating PKCS12 structure.\n");
    return -1;
  }
  p12 = PKCS12_create(NULL, NULL, cert_privkey, cert, NULL, 0, -1, 0, 0, 0);
  if (p12 == NULL )
  {
    printf("Error generating a valid PKCS12 certificate.\n");
    return -1;
  }
  if (!(p12file = fopen(p12cert, "w")))
  {
    printf("Error cant open pkcs12 certificate file for writing.\n");
    return -1;
  }
  bytes = i2d_PKCS12_fp(p12file, p12);
  if (bytes <= 0)
  {
    printf("Error writing PKCS12 certificate.\n");
    return -1;
  }
  fclose(p12file);
  PKCS12_free(p12);
  X509_free(cert);
  EVP_PKEY_free(cert_privkey);

  return 0;
}
#endif
