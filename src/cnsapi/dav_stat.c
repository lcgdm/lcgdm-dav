#include <stdlib.h>
#include "dav_api.h"
#include "dav_private.h"

struct stat_ctx{
	struct Cns_filestat	*statbuf;
	ne_xml_parser		*parser;
	int			iscol;
	size_t			length;
	char			mode[128];			
}; 

int elem_stat_begin(void *userdata, int parent,
                   const char *nspace, const char *name,
                   const char **atts){
	if (strcmp(name, "owner") == 0)
		return PROP_UID;
	else if (strcmp(name, "group") == 0)
		return PROP_GID;
	else if (strcmp(name, "lastaccessed") == 0)
		return PROP_ATIME;
	else if (strcmp(name, "creationdate") == 0)
		return PROP_CTIME;
	else if (strcmp(name, "getlastmodified") == 0)
		return PROP_MTIME;
	else if (strcmp(name, "fileclass") == 0)
		return PROP_FILECLASS;
	else if (strcmp(name, "fileid") == 0)
		return PROP_FILEID;
	else if (strcmp(name, "mode") == 0)
		return PROP_FILEMODE;
	else if (strcmp(name, "getcontentlength") == 0)
		return PROP_LENGTH;
	else if (strcmp(name, "iscollection") == 0)
		return PROP_ISCOL;
	else if (strcmp(name, "prop") == 0)
		return PROP;
	return 1;
}

int elem_stat_data(void *userdata, int state, const char *cdata, size_t len)
{
	struct stat_ctx *ctx = (struct stat_ctx *)userdata;
	struct Cns_filestat *statbuf = ctx->statbuf;

	char buffer[1024];
	memset(buffer, 0, 1024);
	strncpy(buffer, cdata, len);
	switch(state){
		case PROP_UID:
			statbuf->uid = atoi(buffer);
			break;
		case PROP_GID:
			statbuf->gid = atoi(buffer);
			break;	
		case PROP_FILEID:
			statbuf->fileid = atol(buffer);
			break;
		case PROP_FILECLASS:
			statbuf->fileclass = atoi(buffer);
			break;
		case PROP_ATIME:
			statbuf->atime = convert_timestr_to_timet(buffer, RFC2068);
			break;
		case PROP_CTIME:
			statbuf->ctime = convert_timestr_to_timet(buffer, RFC3339);
			break;
		case PROP_MTIME:
			statbuf->mtime = convert_timestr_to_timet(buffer, RFC2068);
			break;
		case PROP_ISCOL:
			ctx->iscol = atoi(buffer);
			break;
		case PROP_LENGTH:
			ctx->length = atol(buffer);
			break;
		case PROP_FILEMODE:
			strcpy(ctx->mode, buffer);
			break;
	}
	return 0;
}

int elem_stat_end(void *userdata, int state, const char *nspace, const char *name)
{
	if(state == PROP)
	{
		struct stat_ctx *ctx = (struct stat_ctx *)userdata;
		struct Cns_filestat *statbuf = ctx->statbuf;

		if(ctx->iscol == 1){
			statbuf->filemode = S_IFDIR;
			statbuf->nlink = ctx->length;
		}else{
			statbuf->filemode = S_IFREG;
			statbuf->nlink = 1;
			statbuf->filesize = ctx->length;
		}
		statbuf->filemode |= (unsigned)strtol(ctx->mode, NULL, 0);
		return 1;
	}

	return 0;
}

int reader_cb(void *userdata, const char *buf, size_t len) {
	struct stat_ctx *ctx = (struct stat_ctx *)userdata;	
	ne_xml_push_handler(ctx->parser, elem_stat_begin, elem_stat_data, elem_stat_end, userdata);
	ne_xml_parse(ctx->parser, buf, len);
	return 0;
}

int Cns_stat(const char *path, struct Cns_filestat *statbuf)
{
  char normalized[CA_MAXPATHLEN];
  
	/* Basic checks for path validity */
	if(normalize_path(path, normalized, sizeof(normalized)) == NULL)
		return -1;

	/* return EFAULT if statbuf is a NULL pointer */
	if(!statbuf)
	{
		errno = EFAULT;
		return -1;
	}

	/* try to open a session if there is no existing one */
	if(!connection)
		if (dav_startsess(NULL,NULL) != NE_OK)
			return -1;

	/* Send a custom request */
	static const char *body = "<propfind xmlns=\"DAV:\">\
				<prop>\
				 <getcontentlength xmlns=\"DAV:\" />\
				 <iscollection xmlns=\"DAV:\" />\
				 <owner xmlns=\"DAV:\" />\
				 <group xmlns=\"DAV:\" />\
				 <fileclass xmlns=\"LCGDM:\"/>\
				 <fileid xmlns=\"LCGDM:\"/>\
				 <mode xmlns=\"LCGDM:\"/>\
				 <status xmlns=\"LCGDM:\"/>\
				 <creationdate xmlns=\"DAV:\" />\
 				 <getlastmodified xmlns=\"DAV:\" />\
				 <lastaccessed xmlns=\"LCGDM:\"/>\
				</prop>\
				 </propfind>";

	/* Request creation */
	ne_request *req;
	req = ne_request_create(connection->session, "PROPFIND", normalized);
	ne_add_request_header(req, "Depth", "0");
	ne_set_request_body_buffer(req, body, strlen(body));

	struct stat_ctx *ctx = (struct stat_ctx *)malloc(sizeof(struct stat_ctx));
	ctx->statbuf = statbuf;
	ctx->parser = ne_xml_create();

	/* Set the XML reader */ 
	ne_add_response_body_reader(req, ne_accept_always, reader_cb, (void *) ctx);

	/* Execute the request (PROPFIND) */
	neon_error = ne_request_dispatch(req);

	errno = 0;

  char *neon_error_code = ne_get_error(connection->session);
  debug_msg("neon_error_code: %s", neon_error_code);
  char *error_req_status = ne_get_status(req);
  debug_msg("error_req_status: %s:", error_req_status);

	char *error_code = get_error_code(neon_error_code);
	if (strcmp(error_code, "403") == 0)
		errno = find403error(normalized);
	else if (strcmp(error_code, "404") == 0)
		errno = ENOENT;
	else if (strcmp(error_code, "405") == 0)
		errno = EEXIST;




	/* Destroy the request and set the end flag */	
	ne_request_destroy(req);
	ne_xml_destroy(ctx->parser);
	free(ctx);

	if(errno != 0 || neon_error != 0)
		return -1;
	return 0;
}


