/* dpns_umask - set and get DPNS file creation mask used by the name server */

#include "dav_api.h"

/* The default value for the umask is 0022 (based on the linux one */ 
__thread mode_t internal_umask = 0022;

/* This function set the umask to a new value and return the last one*/
mode_t Cns_umask(mode_t mask)
{
	mode_t previous_mask = internal_umask;
	internal_umask = 0777 & ~mask;
	return previous_mask;
}
