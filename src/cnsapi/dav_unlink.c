#include "dav_api.h"
#include "dav_private.h"

/* Cns_rmdir - remove a DPNS directory in the name server   
*
*  ERROR:
*  
*  SENOSHOST    : SENOSHOST, SENOSSERV, SECOMERR, ENSNACT (comes with startsess)
*  EFAULT, ENAMETOOLONG : OK (comes with basic_path_checks)
* 
*  Missing error:
*  
*  EINVAL	: current working directory not yet implemented
*/

int Cns_unlink(const char *path)
{
  char normalized[CA_MAXPATHLEN];
  
	/* Basic checks for path validity */
	if(normalize_path(path, normalized, sizeof(normalized)) == NULL)
		return -1;

	/* try to open a connection->session if there is no existing one */
	if(!connection)
		if (dav_startsess(NULL,NULL) != NE_OK)
			return -1;

	if (ne_delete(connection->session, normalized) != NE_OK)
	{
		
		char *error_code = get_error_code(ne_get_error(connection->session));

		if (atoi(error_code) == 403)
			errno = find403error(normalized);
		else if (strcmp(error_code, "404") == 0)
			errno = ENOENT;
		else if (strcmp(error_code, "405") == 0)
			errno = EEXIST;
		else if (atoi(error_code) == 424)
			errno = EACCES;


		return -1;
	}

	return 0;
}
