#include <time.h>
#include "dav_api.h"
#include "dav_private.h"

int Cns_utime(const char *path, struct utimbuf *times)
{
  char normalized[CA_MAXPATHLEN];
  
	/* Basic checks for path validity */
	if(normalize_path(path, normalized, sizeof(normalized)) == NULL)
		return -1;

	/* try to open a session if there is no existing one */
	if(!connection)
		if (dav_startsess(NULL,NULL) != NE_OK)
			return -1;

	time_t 		rawtime;
	struct tm 	*timeinfo;
	char 		strtime[64];
	const char 	davdate_mask[] = "%a, %d %b %Y %H:%M:%S GMT";
	
	time(&rawtime);
	timeinfo = localtime(&rawtime);
	strftime(strtime, 64, davdate_mask, timeinfo);

	/* Try to set the property */
	set_parameter(normalized, "DAV:getlastmodified", strtime);

	timeinfo = localtime(&(times->actime));
	strftime(strtime, 64, davdate_mask, timeinfo);

	set_parameter(normalized, "LCGDM:lastaccessed", strtime);
	
	return 0;
}
