cmake_minimum_required(VERSION 2.6)
# Locate cgreen library in /usr/local/lib
find_library(CGREEN_LIBRARY
		NAME libcgreen.so
		HINTS /usr/local/lib/)

file(GLOB unit_test_SOURCES ../*.c)

if(NOT CGREEN_LIBRARY)
	message(WARNING "Cgreen has to be installed to build unit tests")
else(NOT CGREEN_LIBRARY)
	message(INFO " ${CGREEN_LIBRARY} has been found: unit tests are availables")

	add_definitions(-D_REENTRANT)
	include_directories(${PROJECT_SOURCE_DIR} "${PROJECT_SOURCE_DIR}/build/h")
	add_executable(dav-api-unit-testing ${unit_test_SOURCES})
	add_dependencies(dav-api-unit-testing lcgdmdav)
	target_link_libraries(dav-api-unit-testing dl cgreen lcgdmdav m)
endif(NOT CGREEN_LIBRARY)

		
