#include <sys/types.h>
#include <unistd.h>
#include "test_dpns.h"

/*********************************/
/*                               */
/*  Test for the access function  */
/*                               */
/*********************************/

/* Function in charge of adding all tests to the suite */
void add_access_tests(TestSuite *suite)
{
	add_test(suite, access_test_efault);
        add_test(suite, access_test_enoent);
        add_test(suite, access_test_einval);
        add_test(suite, access_test_enotdir);
        add_test(suite, test_access);
}

/* EFAUL: path is a null pointer */
void access_test_efault()
{
	dpns_access(NULL, R_OK);
	assert_equal(serrno, EFAULT);
}

/* EINVAL: amode is invalid. */
void access_test_einval()
{
	dpns_access("/dpm/cern.ch/home/dteam/", -1);
	if (serrno != EINVAL)
		assert_false(1);

	dpns_access("/dpm/cern.ch/home/dteam/", 8);
        if (serrno != EINVAL)
                assert_false(1);

	serrno = 0;
	dpns_access("/dpm/cern.ch/home/dteam/", (R_OK|W_OK|X_OK));
        if (serrno != 0)
                assert_false(1);

	assert_true(1);

}

/* ENOENT: A component of the path does not exist */
void access_test_enoent()
{
	dpns_access("/dpm/noexistingdir/test/", R_OK);
	assert_equal(serrno, ENOENT);
}

/* ENOTDIR: A component of path prefix is not a directory. */
void access_test_enotdir()
{
	dpns_creat("/dpm/cern.ch/home/dteam/myfile/", 0777);
        dpns_access("/dpm/cern.ch/home/dteam/myfile/test", R_OK);
	dpns_unlink("/dpm/cern.ch/home/dteam/myfile/");
        assert_equal(serrno, ENOTDIR);
}

/* EACCES: Permission problem */
void test_access()
{
	int 	all[8] 		= {F_OK, R_OK, W_OK, X_OK, R_OK|W_OK, R_OK|X_OK, W_OK|X_OK, R_OK|W_OK|X_OK};	
	mode_t 	modes[6] 	= {0111, 0222, 0333, 0444, 0555, 0666};
	int 	successes[6]	= {2, 2, 4, 2, 4, 4};
	int 	total_success;

	/* Test All possible combinaison */
	dpns_mkdir("/dpm/cern.ch/home/dteam/test_directory", 0777);
	int i, j;

	for(j = 0; j < 8; j++)
                if (dpns_access("/dpm/cern.ch/home/dteam/test_directory", all[j]) != 0)
                        assert_false(1);

	for(i = 0; i < 6; i++)
	{
		total_success = 0;
		dpns_chmod("/dpm/cern.ch/home/dteam/test_directory", modes[i]);
        	for(j = 0; j < 8; j++)
		       	if (dpns_access("/dpm/cern.ch/home/dteam/test_directory", all[j]) == 0)
				total_success++;
		if(total_success != successes[i])
			assert_false(1);
	}

	dpns_rmdir("/dpm/cern.ch/home/dteam/test_directory");

	assert_true(1);	

}

