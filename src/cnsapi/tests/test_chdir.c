#include "test_dpns.h"

/*********************************/
/*                               */
/*  Test for the chdid function  */
/*                               */
/*********************************/

/* Function in charge of adding all tests to the suite */
void add_chdir_tests(TestSuite *suite)
{
	add_test(suite, chdir_test_eacces);
	add_test(suite, chdir_test_efault);
	add_test(suite, chdir_test_enoent);
	add_test(suite, chdir_test_enotdir);
	add_test(suite, test_chdir);	
}

/* EACCES: Search permission is denied on a component of the path prefix. */
void chdir_test_eacces()
{
	dpns_mkdir("/dpm/cern.ch/home/dteam/restricted_directory", 0777);
        dpns_mkdir("/dpm/cern.ch/home/dteam/restricted_directory/test", 0777),
        dpns_chmod("/dpm/cern.ch/home/dteam/restricted_directory", 0440);
        dpns_chdir("/dpm/cern.ch/home/dteam/restricted_directory/test");

        /* remove the restricted_directory */
        dpns_chmod("/dpm/cern.ch/home/dteam/restricted_directory", 0777);
        dpns_rmdir("/dpm/cern.ch/home/dteam/restricted_directory/test");
        dpns_rmdir("/dpm/cern.ch/home/dteam/restricted_directory");

	assert_equal(serrno, EACCES);

}

/* EFAUL: path is a null pointer */
void chdir_test_efault()
{
	dpns_chdir(NULL);
	assert_equal(serrno, EFAULT);
}

/* ENOENT: A component of the path does not exist */
void chdir_test_enoent()
{
	dpns_chdir("/dpm/noexistingdir/test/");
	assert_equal(serrno, ENOENT);
}

/* ENOTDIR: A component of the path is not a directory */
void chdir_test_enotdir()
{
	dpns_creat("/dpm/cern.ch/home/dteam/file", 0777);
	dpns_chdir("/dpm/cern.ch/home/dteam/file/test");

	if(serrno != ENOTDIR)
		assert_false(1);

	dpns_chdir("/dpm/cern.ch/home/dteam/file");

	if(serrno != ENOTDIR)
		assert_false(1);

	dpns_unlink("/dpm/cern.ch/home/dteam/file");
}

void test_chdir()
{
	char *buffer;
	buffer = dpns_getcwd(buffer, 1024);

	/* Begin without any current working directory */
	if(buffer)	
		assert_false(1);

	/* Try to chdir to an existing directory */
	dpns_chdir("/dpm/cern.ch/home/dteam");
	buffer = dpns_getcwd(buffer, 1024);
	if(strcmp(buffer, "/dpm/cern.ch/home/dteam") != 0)
		assert_false(1);

	/* Try to go in the parent directory */
	dpns_chdir("..");
	buffer = dpns_getcwd(buffer, 1024);
	if(strcmp(buffer, "/dpm/cern.ch/home") != 0)
		assert_false(1);
	
	/* Try to go back in the child directory */
	dpns_chdir("./home");
	buffer = dpns_getcwd(buffer, 1024);
	if(strcmp(buffer, "/dpm/cern.ch/home") != 0)
		assert_false(1);

	/* Try to go back in the child direcory */
	dpns_chdir("dteam");
	buffer = dpns_getcwd(buffer, 1024);
	if(strcmp(buffer, "/dpm/cern.ch/home/dteam") != 0)
		assert_false(1);

	/* Try a complex combination */
	dpns_chdir("..////./../..///cern.ch//..//");
	buffer = dpns_getcwd(buffer, 1024);
	if(strcmp(buffer, "/dpm") != 0)
		assert_false(1);

	assert_true(1);
}

