#include "test_dpns.h"

/*********************************/
/*                               */
/*  Test for the chmod function  */
/*                               */
/*********************************/

/* Function in charge of adding all tests to the suite */
void add_chmod_tests(TestSuite *suite)
{
	add_test(suite, chmod_test_efault);
	add_test(suite, chmod_test_enoent);
	add_test(suite, chmod_test_enotdir);
	add_test(suite, chmod_test_eacces);
	add_test(suite, test_chmod_directory);
	add_test(suite, test_chmod_file);	
}

/* EACCES: Search permission is denied on a component of the path prefix. */
void chmod_test_eacces()
{
	dpns_mkdir("/dpm/cern.ch/home/dteam/restricted_directory", 0777);
        dpns_mkdir("/dpm/cern.ch/home/dteam/restricted_directory/test", 0777),
        dpns_chmod("/dpm/cern.ch/home/dteam/restricted_directory", 0440);
        dpns_chmod("/dpm/cern.ch/home/dteam/restricted_directory/test", 0777);

        /* remove the restricted_directory */
        dpns_chmod("/dpm/cern.ch/home/dteam/restricted_directory", 0777);
        dpns_rmdir("/dpm/cern.ch/home/dteam/restricted_directory/test");
        dpns_rmdir("/dpm/cern.ch/home/dteam/restricted_directory");

	assert_equal(serrno, EACCES);

}

/* EFAUL: path is a null pointer */
void chmod_test_efault()
{
	dpns_chmod(NULL, 0777);
	assert_equal(serrno, EFAULT);
}

/* ENOENT: A component of the path does not exist */
void chmod_test_enoent()
{
	dpns_chmod("/dpm/noexistingdir/test/", 0777);
	assert_equal(serrno, ENOENT);
}

/* ENOTDIR: A component of the path is not a directory */
void chmod_test_enotdir()
{
	dpns_creat("/dpm/cern.ch/home/dteam/file", 0777);
	dpns_chmod("/dpm/cern.ch/home/dteam/file/test", 0755);
	dpns_unlink("/dpm/cern.ch/home/dteam/file");
	assert_equal(serrno, ENOTDIR);

}

/* Test to change directory permission */
void test_chmod_directory()
{
	struct dpns_filestat statbuf;
	
	dpns_mkdir("/dpm/cern.ch/home/dteam/testchmod/", 0777);
	dpns_chmod("/dpm/cern.ch/home/dteam/testchmod/", 0755);
	dpns_stat("/dpm/cern.ch/home/dteam/testchmod/", &statbuf);
	mode_t mode = statbuf.filemode;
	dpns_rmdir("/dpm/cern.ch/home/dteam/testmode/");
	assert_equal(mode, 16877);
}

/* Test to change file permission */
void test_chmod_file()
{
	struct dpns_filestat statbuf;

	dpns_creat("/dpm/cern.ch/home/dteam/testchmod", 0777);
	dpns_chmod("/dpm/cern.ch/home/dteam/testchmod", 0521);
	dpns_stat("/dpm/cern.ch/home/dteam/testchmod", &statbuf);
	mode_t mode = statbuf.filemode;
	dpns_unlink("/dpm/cern.ch/home/dteam/testchmod");
	assert_equal(mode, 16721);
}
