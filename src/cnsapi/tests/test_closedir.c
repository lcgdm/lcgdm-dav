#include "test_dpns.h"

/************************************/
/*                                  */
/*  Test for the closedir function  */
/*                                  */
/************************************/

/* Function in charge of adding all tests to the suite */
void add_closedir_tests(TestSuite *suite)
{
	add_test(suite, closedir_test_efault);
	add_test(suite, test_closedir);
}

/* EFAUL: path is a null pointer */
void closedir_test_efault()
{
	dpns_closedir(NULL);
	assert_equal(serrno, EFAULT);
}

void test_closedir()
{
	dpns_DIR *dir = dpns_opendir("/dpm/cern.ch/home/dteam/");

	if(!dir)
		assert_false(1);

	if(dpns_closedir(dir) == 0)
		assert_true(1);
	else
		assert_false(1);

}

