#include "test_dpns.h"

/*********************************/
/*                               */
/*  Test for the creat function  */
/*                               */
/*********************************/

/* Function in charge of adding all tests to the suite */
void add_creat_tests(TestSuite *suite)
{
	add_test(suite, creat_test_eacces);
        add_test(suite, creat_test_efault);
	add_test(suite, creat_test_eisdir);
        add_test(suite, creat_test_enoent);
        add_test(suite, creat_test_enotdir);
        add_test(suite, test_creat);
}

/* EACCES: Permission problem */
void creat_test_eacces()
{
	/* Case 1: File doesn't exist and no write permission in parent directory */
	dpns_mkdir("/dpm/cern.ch/home/dteam/restricted_directory", 0440);
	dpns_creat("/dpm/cern.ch/home/dteam/restricted_directory/test", 0777);
	dpns_chmod("/dpm/cern.ch/home/dteam/restricted_directory", 0777);
	dpns_rmdir("/dpm/cern.ch/home/dteam/restricted_directory/");
	if (serrno != EACCES)
		assert_false(1);	

	/* Case 2: File exists but doesn't have write permission */
	serrno = 0;
	dpns_creat("/dpm/cern.ch/home/dteam/existing_file", 0400);
	dpns_creat("/dpm/cern.ch/home/dteam/existing_file", 0777);
	dpns_unlink("/dpm/cern.ch/home/dteam/existing_file");
	if (serrno != EACCES)
		assert_false(1);


}

/* EFAUL: path is a null pointer */
void creat_test_efault()
{
	dpns_creat(NULL, 0777);
	assert_equal(serrno, EFAULT);
}

/* EISDIR */
void creat_test_eisdir()
{
	dpns_mkdir("/dpm/cern.ch/home/dteam/existing_dir/", 0777);
	dpns_creat("/dpm/cern.ch/home/dteam/existing_dir", 0777);
	dpns_rmdir("/dpm/cern.ch/home/dteam/existing_dir/");
	assert_equal(serrno, EISDIR);
}

/* ENOENT: A component of the path does not exist */
void creat_test_enoent()
{
	dpns_creat("/dpm/noexistingdir/test/", 0777);
	assert_equal(serrno, ENOENT);
}



/* ENOTDIR: A component of the path is not a directory */
/* TODO rewrite to autoaticaly add the file */
void creat_test_enotdir()
{
	dpns_creat("/dpm/cern.ch/home/dteam/file", 0777);
	dpns_creat("/dpm/cern.ch/home/dteam/file/test", 0777);
	dpns_unlink("/dpm/cern.ch/home/dteam/file");
	assert_equal(serrno, ENOTDIR);
}

/* This function test if the mkdir function is working fine */
void test_creat()
{
	dpns_creat("/dpm/cern.ch/home/dteam/testfile", 0777);

	struct dpns_filestat statbuf;

	dpns_stat("/dpm/cern.ch/home/dteam/testfile", &statbuf);
	int iscollection = S_ISREG(statbuf.filemode);
	dpns_unlink("/dpm/cern.ch/home/dteam/testfile");
	assert_true(iscollection);

}

