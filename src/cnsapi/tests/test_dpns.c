#include "test_dpns.h"

#include <sys/param.h>
#include <stdlib.h>
#include <unistd.h>

#define EMPTY 	0
#define FULL	1

int main(int argc, char *argv[])
{
       // dpns_startsess("lxfsra04a04", NULL);

	
/*	int optchar;

	int type = 0;
	char size[8];

	while((optchar = getopt(argc, argv, "t:s:")) != -1)
	{
		switch(optchar)
		{
			case 't':
				type = atoi(optarg);
				break;
			case 's':
				strcpy(size,  optarg);
				break;
			default:
				break;
		}
	}

	struct timeval start, end;
        long seconds, useconds;
	gettimeofday(&start, NULL);

	char buffer[128];
	sprintf(buffer, "/dpm/cern.ch/home/dteam/dir%s", size);

	dpns_DIR *dir = dpns_opendir(buffer);

	if(type == EMPTY)
	{
		struct dirent *entry;
		while((entry = dpns_readdir(dir)))
			printf("%s\n", entry->d_name);
	}
	else if(type == FULL)
	{
		struct dpns_direnstat *entry2;
		while((entry2 = dpns_readdirx(dir)))
		{
			printf("%s (%d)\n", entry2->d_name, entry2->d_reclen);
			printf("\tuid\t= %d\n\tgid\t= %d\n", entry2->uid, entry2->gid);
			printf("\tfileid\t= %ld\n", entry2->fileid);
			printf("\tmode\t= %ld\n", entry2->filemode);
			printf("\tclass\t= %d\n", entry2->fileclass);
			if(entry2->filesize)
				printf("\tfilesize= %d\n", entry2->filesize);
			if(entry2->nlink)
                        	printf("\tnlink\t= %d\n", entry2->nlink);
			printf("\tatime\t= %ld\n", entry2->atime);
                	printf("\tatime\t= %ld\n", entry2->mtime);
			printf("\n");
		}
	}

	dpns_closedir(dir);

	gettimeofday(&end, NULL);

        seconds  = end.tv_sec  - start.tv_sec;
        useconds = end.tv_usec - start.tv_usec;
	double microtime = seconds * 1000000 + useconds;
        float stime = microtime / 1000000;

	printf("time:%f\n", stime);
*/
	dpns_startsess("lxfsra04a04", NULL);

	TestSuite *suite = create_test_suite();

	add_access_tests(suite);
	add_chdir_tests(suite);
	add_chmod_tests(suite);
	add_closedir_tests(suite);
	add_creat_tests(suite);
	add_getcwd_tests(suite);
	add_mkdir_tests(suite);
	add_opendir_tests(suite);
	add_readdir_tests(suite);
	add_rmdir_tests(suite);
	add_stat_tests(suite);
	add_unlink_tests(suite);

	run_test_suite(suite, create_cute_reporter());	

	dpns_endsess();
	return 0;
}

