#ifndef H_TEST_DPNS
#define H_TEST_DPNS

#include <stdio.h>
#include <time.h>

#include <errno.h> 
#include <string.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>

#include "cgreen/cgreen.h"
#include "dpns_api.h"

#include "serrno.h"

/* Prototype of adding test to the suite */
void add_access_tests(TestSuite *suite);
void add_chdir_tests(TestSuite *suite);
void add_chmod_tests(TestSuite *suite);
void add_closedir_tests(TestSuite *suite);
void add_creat_tests(TestSuite *suite);
void add_getcwd_tests(TestSuite *suite);
void add_mkdir_tests(TestSuite *suite);
void add_opendir_tests(TestSuite *suite);
void add_readdir_tests(TestSuite *suite);
void add_stat_tests(TestSuite *suite);
void add_unlink_tests(TestSuite *suite);

/* Prototype for access tests */
void access_test_efault();
void access_test_einval();
void access_test_enoent();
void access_test_enotdir();
void test_access();

/* Prototype for chmod tests */
void chdir_test_eacces();
void chdir_test_efault();
void chdir_test_enoent();
void chdir_test_enotdir();
void test_chdir();

/* Prototype for chmod tests */
void chmod_test_eacces();
void chmod_test_efault();
void chmod_test_enoent();
void chmod_test_enotdir();
void test_chmod_directory();
void test_chmod_file();

/* Prototype for closedir tests */
void closedir_test_efault();
void test_closedir();

/* Prototype for creat tests*/
void creat_test_eacces();
void creat_test_efault();
void creat_test_eisdir();
void creat_test_enoent();
void creat_test_enotdir();
void test_creat();

/* Prototype for getcwd tests */
void getcwd_test_einval();
void getcwd_test_enoent();
void getcwd_test_erange();

/* Prototype for mkdir tests*/
void mkdir_test_eacces();
void mkdir_test_eexist();
void mkdir_test_efault();
void mkdir_test_enoent();
void mkdir_test_enotdir();
void test_mkdir();

/* Prototype for opendir tests (and derivative method) */ 
void opendir_test_eacces();
void opendir_test_efault();
void opendir_test_enoent();
void opendir_test_enotdir();
void test_opendir();

/* Prototype for readdir tests */
void readdir_test_efault();
void test_readdir();

/* Prototype for rmdir tests */ 
void rmdir_test_eacces();
void rmdir_test_eexist();
void rmdir_test_efault();
void rmdir_test_enoent();
void rmdir_test_enotdir();
void test_rmdir();

/* Prototype for stat tests */
void stat_test_eacces();
void stat_test_efault();
void stat_test_enoent();
void stat_test_enotdir();
void test_stat_directory();
void test_stat_file();

/* Prototype of rmdir tests */ 
void unlink_test_eacces();
void unlink_test_efault();
void unlink_test_enoent();
void unlink_test_enotdir();
void test_unlink();
#endif
