#include "test_dpns.h"

/*********************************/
/*                               */
/*  Test for the getcwd function */
/*                               */
/*********************************/

/* Function in charge of adding all tests to the suite */
void add_getcwd_tests(TestSuite *suite)
{
	add_test(suite, getcwd_test_einval);
	add_test(suite, getcwd_test_enoent);
	add_test(suite, getcwd_test_erange);
}

void getcwd_test_enoent()
{
	dpns_getcwd(NULL, 1024);
	assert_equal(serrno, ENOENT);
}

void getcwd_test_einval()
{
	dpns_chdir("/dpm");
	dpns_getcwd(NULL, 0);
	assert_equal(serrno, EINVAL);	
}

void getcwd_test_erange()
{
	dpns_chdir("/dpm");
	dpns_getcwd(NULL, 3);
	assert_equal(serrno, ERANGE);
}
