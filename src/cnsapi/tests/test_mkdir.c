#include "test_dpns.h"

/*********************************/
/*                               */
/*  Test for the mkdir function  */
/*                               */
/*********************************/

/* Function in charge of adding all tests to the suite */
void add_mkdir_tests(TestSuite *suite)
{
	add_test(suite, mkdir_test_eacces);
	add_test(suite, mkdir_test_eexist);
	add_test(suite, mkdir_test_efault);
	add_test(suite, mkdir_test_enoent);
	add_test(suite, mkdir_test_enotdir);
	add_test(suite, test_mkdir);
}

/* EACCES: Permission problem */
void mkdir_test_eacces()
{
	dpns_mkdir("/dpm/cern.ch/home/dteam/restricted_directory/", 0440);
	dpns_mkdir("/dpm/cern.ch/home/dteam/restricted_directory/test", 0777);
	dpns_chmod("/dpm/cern.ch/home/dteam/restricted_directory", 0777);
	dpns_rmdir("/dpm/cern.ch/home/dteam/restricted_directory");
	assert_equal(serrno, EACCES);
}

/* EEXIST: Path already exist */
void mkdir_test_eexist()
{
	/* Directory already exist */
	dpns_mkdir("/dpm/cern.ch/home/dteam/existing_directory", 0777);
	dpns_mkdir("/dpm/cern.ch/home/dteam/existing_directory", 0777);
	dpns_rmdir("/dpm/cern.ch/home/dteam/existing_directory/");
	if (serrno != EEXIST)
		assert_false(1);

	/* File already exist */
	dpns_creat("/dpm/cern.ch/home/dteam/existing_file", 0777);
	dpns_mkdir("/dpm/cern.ch/home/dteam/existing_file", 0777);
	dpns_unlink("/dpm/cern.ch/home/dteam/existing_file");
	if (serrno != EEXIST)
		assert_false(1);

}

/* EFAUL: path is a null pointer */
void mkdir_test_efault()
{
	dpns_mkdir(NULL, 0777);
	assert_equal(serrno, EFAULT);
}

/* ENOENT: A component of the path does not exist */
void mkdir_test_enoent()
{
	dpns_mkdir("/dpm/noexistingdir/test/", 0777);
	assert_equal(serrno, ENOENT);
}


/* ENOTDIR: A component of the path is not a directory */
void mkdir_test_enotdir()
{
	dpns_creat("/dpm/cern.ch/home/dteam/file", 0777);
	dpns_mkdir("/dpm/cern.ch/home/dteam/file/test", 0777);
	dpns_unlink("/dpm/cern.ch/home/dteam/file");
	assert_equal(serrno, ENOTDIR);
}

/* This function test if the mkdir function is working fine */
void test_mkdir()
{
	dpns_rmdir("/dpm/cern.ch/home/dteam/test");
	dpns_mkdir("/dpm/cern.ch/home/dteam/test", 0777);

	struct dpns_filestat statbuf;

	dpns_stat("/dpm/cern.ch/home/dteam/test/", &statbuf);
	int iscollection = S_ISDIR(statbuf.filemode);
	dpns_rmdir("/dpm/cern.ch/home/dteam/test");
	assert_true(iscollection);
}

