#include "test_dpns.h"

/*********************************/
/*                               */
/*  Test for the opendir function  */
/*                               */
/*********************************/

/* Function in charge of adding all tests to the suite */
void add_opendir_tests(TestSuite *suite)
{
	add_test(suite, opendir_test_eacces);
	add_test(suite, opendir_test_efault);
	add_test(suite, opendir_test_enoent);
	add_test(suite, opendir_test_enotdir);
	add_test(suite, test_opendir);
}

void opendir_test_eacces()
{
	dpns_mkdir("/dpm/cern.ch/home/dteam/restricted_directory/", 0111);
	dpns_opendir("/dpm/cern.ch/home/dteam/restricted_directory/");

	dpns_chmod("/dpm/cern.ch/home/dteam/restricted_directory/", 0777);
	dpns_rmdir("/dpm/cern.ch/home/dteam/restricted_directory/");

	assert_equal(serrno, EACCES);
}

void opendir_test_efault()
{
	dpns_opendir(NULL);
	assert_equal(serrno, EFAULT);
}

void opendir_test_enoent()
{
	dpns_opendir("/dpm/notexistingdir/test");
	assert_equal(serrno, ENOENT);
}

void opendir_test_enotdir()
{
	dpns_creat("/dpm/cern.ch/home/dteam/file", 0777);
	dpns_opendir("/dpm/cern.ch/home/dteam/file/test");
	dpns_unlink("/dpm/cern.ch/home/dteam/file");
	assert_equal(serrno, ENOTDIR);
}

void test_opendir()
{
	dpns_DIR *dir = dpns_opendir("/dpm/cern.ch/home/dteam/");

	int opened = 0;
	if(dir)
		opened = 1;

	assert_true((int)opened);
}
