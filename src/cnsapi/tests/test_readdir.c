#include "test_dpns.h"

/***********************************/
/*                                 */
/*  Test for the readdir function  */
/*                                 */
/***********************************/

/* Function in charge of adding all tests to the suite */
void add_readdir_tests(TestSuite *suite)
{
	add_test(suite, readdir_test_efault);
	add_test(suite, test_readdir);
}

/* EFAUL: path is a null pointer */
void readdir_test_efault()
{
	dpns_readdir(NULL);
	assert_equal(serrno, EFAULT);
}

void test_readdir()
{
	dpns_mkdir("/dpm/cern.ch/home/dteam/dir/",0777);
	int i;

	for(i=0;i<10;i++)
	{
		char buffer[256];
		sprintf(buffer, "/dpm/cern.ch/home/dteam/dir/%d", i);
		dpns_mkdir(buffer, 0777);
	}

	dpns_DIR *dir =	dpns_opendir("/dpm/cern.ch/home/dteam/dir/");

	struct dirent *entry;

	int files = 0;

	while((entry = dpns_readdir(dir)))
		files++;

	dpns_closedir(dir);


	for(i=0;i<10;i++)
	{
		char buffer[256];
		sprintf(buffer, "/dpm/cern.ch/home/dteam/dir/%d", i);
		dpns_rmdir(buffer);
	}
	dpns_rmdir("/dpm/cern.ch/home/dteam/dir/");

	if(files == 10)
		assert_true(1);
	else
		assert_false(1);
}
