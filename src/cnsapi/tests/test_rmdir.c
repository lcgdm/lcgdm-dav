#include "test_dpns.h"

/*********************************/
/*                               */
/*  Test for the rmdir function  */
/*                               */
/*********************************/

/* Function in charge of adding all tests to the suite */
void add_rmdir_tests(TestSuite *suite)
{
	add_test(suite, rmdir_test_eacces);
	add_test(suite, rmdir_test_eexist);
	add_test(suite, rmdir_test_efault);
	add_test(suite, rmdir_test_enoent);
	add_test(suite, rmdir_test_enotdir);
	add_test(suite, test_rmdir);
}

/* EACCES: try to remove a elem in a directory without the correct right */
void rmdir_test_eacces()
{
	dpns_mkdir("/dpm/cern.ch/home/dteam/restricted_directory", 0777);
        dpns_mkdir("/dpm/cern.ch/home/dteam/restricted_directory/test", 0777);
        dpns_chmod("/dpm/cern.ch/home/dteam/restricted_directory", 0440);
        dpns_rmdir("/dpm/cern.ch/home/dteam/restricted_directory/test");
        dpns_chmod("/dpm/cern.ch/home/dteam/restricted_directory", 0777);
       	dpns_rmdir("/dpm/cern.ch/home/dteam/restricted_directory/test");
        dpns_rmdir("/dpm/cern.ch/home/dteam/restricted_directory");
	assert_equal(serrno, EACCES);
}

void rmdir_test_eexist()
{
	dpns_mkdir("/dpm/cern.ch/home/dteam/nonempty_directory", 0777);
	dpns_mkdir("/dpm/cern.ch/home/dteam/nonempty_directory/dir", 0777);
	dpns_rmdir("/dpm/cern.ch/home/dteam/nonempty_directory/");
	dpns_rmdir("/dpm/cern.ch/home/dteam/nonempty_directory/dir");
	dpns_rmdir("/dpm/cern.ch/home/dteam/nonempty_directory/");
	assert_equal(serrno, EEXIST);
}

/* EFAUL: path is a null pointer */
void rmdir_test_efault()
{
	dpns_rmdir(NULL);
	assert_equal(serrno, EFAULT);
}

/* ENOENT: A component of the path does not exist */
void rmdir_test_enoent()
{
	dpns_rmdir("/dpm/noexistingdir/test/");
	assert_equal(serrno, ENOENT);
}


void rmdir_test_enotdir()
{
	dpns_creat("/dpm/cern.ch/home/dteam/file/", 0775);
	dpns_rmdir("/dpm/cern.ch/home/dteam/file/test");
	dpns_unlink("/dpm/cern.ch/home/dteam/file/");
	assert_equal(serrno, ENOTDIR);
}

void test_rmdir()
{
	dpns_mkdir("/dpm/cern.ch/home/dteam/test", 0777);
	dpns_mkdir("/dpm/cern.ch/home/dteam/test/test", 0777);

	struct dpns_filestat statbuf;

	dpns_stat("/dpm/cern.ch/home/dteam/test", &statbuf);

	int elem = statbuf.nlink;
	if(elem != 1)
		assert_false(1);
	dpns_rmdir("/dpm/cern.ch/home/dteam/test/test");

	dpns_stat("/dpm/cern.ch/home/dteam/test", &statbuf);
	elem = statbuf.nlink;
	dpns_rmdir("/dpm/cern.ch/home/dteam/test");

	assert_false(elem);
}

