#include "test_dpns.h"

/*********************************/
/*                               */
/*  Test for the chmod function  */
/*                               */
/*********************************/

/* Function in charge of adding all tests to the suite */
void add_stat_tests(TestSuite *suite)
{
	add_test(suite, stat_test_eacces);
	add_test(suite, stat_test_efault);
	add_test(suite, stat_test_enoent);
	add_test(suite, stat_test_enotdir);
	add_test(suite, test_stat_directory);
	add_test(suite, test_stat_file);
}

void stat_test_eacces()
{
	struct dpns_filestat statbuf;

	dpns_mkdir("/dpm/cern.ch/home/dteam/restricted_directory", 0777);
        dpns_mkdir("/dpm/cern.ch/home/dteam/restricted_directory/test", 0777),
        dpns_chmod("/dpm/cern.ch/home/dteam/restricted_directory", 0400);
	dpns_stat("/dpm/cern.ch/home/dteam/restricted_directory/test", &statbuf);
        dpns_chmod("/dpm/cern.ch/home/dteam/restricted_directory", 0777);
        dpns_rmdir("/dpm/cern.ch/home/dteam/restricted_directory/test");
        dpns_rmdir("/dpm/cern.ch/home/dteam/restricted_directory");
	assert_equal(serrno, EACCES);
}

/* EFAUL: path is a null pointer */
void stat_test_efault()
{
	dpns_stat(NULL, NULL);
	assert_equal(serrno, EFAULT);
}

/* ENOENT: A component of the path does not exist */
void stat_test_enoent()
{
	struct dpns_filestat statbuf;
	
	dpns_stat("/dpm/noexistingdir/test/", &statbuf);
	assert_equal(serrno, ENOENT);
}


/*ENOTDIR: A component of the path is not a directory */
void stat_test_enotdir()
{
	struct dpns_filestat statbuf;
	
	dpns_creat("/dpm/cern.ch/home/dteam/existingfile/", 0777);
	dpns_stat("/dpm/cern.ch/home/dteam/existingfile/test", &statbuf);
	assert_equal(serrno, ENOTDIR);
}

/* Time (a, c, m) are not tested by the function*/
void test_stat_directory()
{
	dpns_mkdir("/dpm/cern.ch/home/dteam/test",0755);
	time_t now = time(NULL);

	struct dpns_filestat statbuf;
	dpns_stat("/dpm/cern.ch/home/dteam/test", &statbuf);

	if((!statbuf.uid) || (!statbuf.gid))
		assert_false(1);
	if((!statbuf.fileid) && (!statbuf.fileclass))
		assert_false(1);
	if(!S_ISDIR(statbuf.filemode))
		assert_false(1);
	if(statbuf.nlink != 0)
		assert_false(1);

	dpns_rmdir("/dpm/cern.ch/home/dteam/test");
	assert_true(1);
}

void test_stat_file()
{
	dpns_creat("/dpm/cern.ch/home/dteam/test",0755);
	time_t now = time(NULL);

	struct dpns_filestat statbuf;
	dpns_stat("/dpm/cern.ch/home/dteam/test", &statbuf);

	if((!statbuf.uid) || (!statbuf.gid))
		assert_false(1);
	if((!statbuf.fileid) && (!statbuf.fileclass))
		assert_false(1);
	if(!S_ISREG(statbuf.filemode))
		assert_false(1);

	dpns_unlink("/dpm/cern.ch/home/dteam/test");

	assert_true(1);
}
