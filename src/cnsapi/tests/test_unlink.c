#include "test_dpns.h"

/*********************************/
/*                               */
/*  Test for the unlink function */
/*                               */
/*********************************/

/* Function in charge of adding all tests to the suite */
void add_unlink_tests(TestSuite *suite)
{
	add_test(suite, unlink_test_eacces);
        add_test(suite, unlink_test_efault);
        add_test(suite, unlink_test_enoent);
        add_test(suite, unlink_test_enotdir);
        add_test(suite, test_unlink);
}

void unlink_test_eacces()
{
	dpns_mkdir("/dpm/cern.ch/home/dteam/restricted_directory", 0777);
        dpns_creat("/dpm/cern.ch/home/dteam/restricted_directory/test", 0777);
        dpns_chmod("/dpm/cern.ch/home/dteam/restricted_directory", 0440);
        dpns_unlink("/dpm/cern.ch/home/dteam/restricted_directory/test");
        dpns_chmod("/dpm/cern.ch/home/dteam/restricted_directory", 0777);
       	dpns_unlink("/dpm/cern.ch/home/dteam/restricted_directory/test");
        dpns_rmdir("/dpm/cern.ch/home/dteam/restricted_directory");
	assert_equal(serrno, EACCES);
}

void unlink_test_efault()
{
	dpns_unlink(NULL);
	assert_equal(serrno, EFAULT);
}

void unlink_test_enoent()
{
	dpns_unlink("/dpm/noexistingdir/test/");
	assert_equal(serrno, ENOENT);
}


void unlink_test_enotdir()
{
	dpns_creat("/dpm/cern.ch/home/dteam/file",0777);
	dpns_unlink("/dpm/cern.ch/home/dteam/file/test");
	dpns_unlink("/dpm/cern.ch/home/dteam/file");
	assert_equal(serrno, ENOTDIR);
}

void test_unlink()
{
	dpns_mkdir("/dpm/cern.ch/home/dteam/test", 0777);
	dpns_creat("/dpm/cern.ch/home/dteam/test/test", 0777);

	struct dpns_filestat statbuf;

	dpns_stat("/dpm/cern.ch/home/dteam/test", &statbuf);

	int elem = statbuf.nlink;
	if(elem != 1)
		assert_false(1);
	dpns_unlink("/dpm/cern.ch/home/dteam/test/test");

	dpns_stat("/dpm/cern.ch/home/dteam/test", &statbuf);
	elem = statbuf.nlink;
	dpns_rmdir("/dpm/cern.ch/home/dteam/test");

	assert_false(elem);
}

