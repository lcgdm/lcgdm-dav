/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <apr.h>
#include <apr_pools.h>
#include <apr_buckets.h>
#include <dmlite/c/io.h>

#include "dmlite_bucket.h"

static apr_bucket* dmlite_bucket_make(apr_bucket *b, dmlite_fd *fd,
        apr_off_t offset, apr_size_t length, apr_pool_t *p)
{
    apr_bucket_dmlite *f;

    f = apr_bucket_alloc(sizeof(*f), b->list);
    f->fd = fd;
    f->pool = p;

    b = apr_bucket_shared_make(b, f, offset, length);
    b->type = &apr_bucket_type_dmlite;

    return b;
}

apr_bucket* dmlite_bucket_create(dmlite_fd *fd, apr_off_t offset,
        apr_size_t length, apr_pool_t *p, apr_bucket_alloc_t *list)
{
    apr_bucket *b = apr_bucket_alloc(sizeof(*b), list);

    APR_BUCKET_INIT(b);
    b->free = apr_bucket_free;
    b->list = list;
    return dmlite_bucket_make(b, fd, offset, length, p);
}

apr_bucket* apr_brigade_insert_dmlite(apr_bucket_brigade *bb, dmlite_fd *fd,
        apr_off_t start, apr_off_t length, apr_pool_t *p)
{
    /* Code adapted from apr_brigade_insert_file */
    apr_bucket *e;

    if (sizeof(apr_off_t) == sizeof(apr_size_t) || length < MAX_BUCKET_SIZE) {
        e = dmlite_bucket_create(fd, start, length, p, bb->bucket_alloc);
    }
    else {
        e = dmlite_bucket_create(fd, start, MAX_BUCKET_SIZE, p,
                bb->bucket_alloc);

        while (length > MAX_BUCKET_SIZE) {
            apr_bucket *ce;
            apr_bucket_copy(e, &ce);
            APR_BRIGADE_INSERT_TAIL(bb, ce);
            e->start += MAX_BUCKET_SIZE;
            length -= MAX_BUCKET_SIZE;
        }

        e->length = (apr_size_t) length;
    }

    APR_BRIGADE_INSERT_TAIL(bb, e);
    return e;
}

static void dmlite_bucket_destroy(void *data)
{
    apr_bucket_dmlite *f = (apr_bucket_dmlite*) data;

    if (apr_bucket_shared_destroy(f)) {
        apr_bucket_free(f);
    }
}

static apr_status_t dmlite_bucket_read(apr_bucket *e, const char **str,
        apr_size_t *len, apr_read_type_e block)
{
    (void) block;

    apr_bucket_dmlite *a = (apr_bucket_dmlite*) e->data;
    dmlite_fd *fd = a->fd;
    apr_bucket *b;
    char *buf;
    apr_size_t filelength = e->length;
    apr_off_t fileoffset = e->start;
    ssize_t readsize;

    *len = (filelength > MAX_BUCKET_SIZE) ? MAX_BUCKET_SIZE : filelength;
    *str = NULL;
    buf = apr_bucket_alloc(*len, e->list);

    /* Offset and read */
    if (dmlite_fseek(fd, fileoffset, SEEK_SET) != 0) {
        apr_bucket_free(buf);
        return APR_EGENERAL;
    }
    readsize = dmlite_fread(fd, buf, *len);
    if (readsize < 0) {
        apr_bucket_free(buf);
        return APR_EGENERAL;
    }
    *len = readsize;
    filelength -= *len;

    /** Change the current bucket */
    apr_bucket_heap_make(e, buf, *len, apr_bucket_free);

    /* Create another bucket if there is more to read */
    if (filelength > 0 && !dmlite_feof(fd)) {
        b = apr_bucket_alloc(sizeof(*b), e->list);
        b->start = fileoffset + (*len);
        b->length = filelength;
        b->data = a;
        b->type = &apr_bucket_type_dmlite;
        b->free = apr_bucket_free;
        b->list = e->list;
        APR_BUCKET_INSERT_AFTER(e, b);
    }
    else {
        dmlite_bucket_destroy(a);
    }

    *str = buf;
    return APR_SUCCESS;
}

static apr_status_t dmlite_bucket_setaside(apr_bucket *data,
        apr_pool_t *reqpool)
{
    apr_bucket_dmlite *a = (apr_bucket_dmlite*) data->data;

    if (!apr_pool_is_ancestor(a->pool, reqpool))
        a->pool = reqpool;

    return APR_SUCCESS;
}

APR_DECLARE_DATA const apr_bucket_type_t apr_bucket_type_dmlite =
{
    "DMLITE",
    5,
    APR_BUCKET_DATA,
    dmlite_bucket_destroy,
    dmlite_bucket_read,
    dmlite_bucket_setaside,
    apr_bucket_shared_split,
    apr_bucket_shared_copy
};
