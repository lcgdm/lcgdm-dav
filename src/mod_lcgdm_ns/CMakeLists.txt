cmake_minimum_required (VERSION 2.6)

find_library(JSON_C_LIBRARIES
    NAMES json json-c
    HINTS /usr/lib /usr/lib64 /lib /lib64
    DOC "json-c library"
)
find_path(JSON_C_INCLUDE_DIR
    NAMES json.h
    HINTS /usr/include/json
    HINTS /usr/include/json-c
    DOC "json-c headers"
)

include_directories(
    ${JSON_C_INCLUDE_DIR}
    ${CMAKE_SOURCE_DIR}/external/libmacaroons
    ${CMAKE_CURRENT_SOURCE_DIR}
)

add_library(mod_lcgdm_ns MODULE
    acl.c
    checksum.c
    dbm.c
    delivery.c
    liveprops.c
    mime.c
    mod_lcgdm_ns.c
    repository.c
    replicas.c
    macaroons.c
    ../shared/checksum.c
    ../shared/security.c
    ../shared/delegation.c
    ../shared/utils.c
)

set_target_properties(mod_lcgdm_ns PROPERTIES PREFIX "")
target_link_libraries(mod_lcgdm_ns ${DMLITE_LIBRARIES} ${JSON_C_LIBRARIES} macaroons)

install(TARGETS     mod_lcgdm_ns
        DESTINATION usr/lib${LIB_SUFFIX}/httpd/modules)
