/*
 * Copyright (c) CERN 2017
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string.h>

#define __USE_XOPEN
#include <time.h>

#include <apr_file_io.h>
#include <dmlite/c/dmlite.h>
#include <dmlite/c/catalog.h>
#include <dmlite/c/pool.h>

#include <httpd.h>
#include <http_protocol.h>
#include <macaroons.h>
#include <mod_dav.h>
#include <json.h>
#include <apr_strings.h>
#include <http_log.h>
#include "mod_lcgdm_ns.h"
#include "macaroons.h"

typedef struct {
    activity_t activity;
    const char *repr;
} activity_map_t;

static const activity_map_t activity_map[] = {
    {ACTIVITY_DOWNLOAD, "DOWNLOAD"},
    {ACTIVITY_UPLOAD, "UPLOAD"},
    {ACTIVITY_LIST, "LIST"},
    {ACTIVITY_DELETE, "DELETE"},
    {ACTIVITY_MANAGE, "MANAGE"},
    {ACTIVITY_NONE, NULL}
};

/**
 * Convert an activities enum to its string representation
 * @param p         It uses this pool to allocate the return string
 * @param activities
 * @return          A string representing the flags 'activities'
 */
static const char* activites_val_to_str(apr_pool_t *p, activity_t activities)
{
    const char *repr = "";
    int i = 0;

    for (i = 0; activity_map[i].activity; ++i) {
        if (activities & activity_map[i].activity) {
            repr = apr_pstrcat(p, repr, activity_map[i].repr, ",", NULL);
        }
    }

    return repr;
}

/**
 * Convert the string representation of a list of activities to its enum representation
 * @param str The string to be converted
 * @return The converted value
 */
static activity_t activities_str_to_val(const char *str)
{
    activity_t activities = ACTIVITY_NONE;
    char *mutable = strdup(str), *saveptr = NULL, *token;

    for (token = strtok_r(mutable, ",", &saveptr); token; token = strtok_r(NULL, ",", &saveptr)) {
        int i;
        for (i = 0; activity_map[i].activity; ++i) {
            if (strcasecmp(token, activity_map[i].repr) == 0) {
                activities |= activity_map[i].activity;
            }
        }
    }

    free(mutable);
    return activities;
}


/**
 * From https://httpd.apache.org/docs/2.4/developer/modguide.html
 */
static int read_body(request_rec *r, char **rbuf, apr_off_t *size)
{
    int rc = 0;

    if((rc = ap_setup_client_block(r, REQUEST_CHUNKED_ERROR))) {
        return rc;
    }

    if(ap_should_client_block(r)) {
        char         argsbuffer[HUGE_STRING_LEN];
        apr_off_t    rsize, len_read, rpos = 0;
        apr_off_t length = r->remaining;

        *rbuf = (char *) apr_pcalloc(r->pool, (apr_size_t) (length + 1));
        *size = length;
        while((len_read = ap_get_client_block(r, argsbuffer, sizeof(argsbuffer))) > 0) {
            if((rpos + len_read) > length) {
                rsize = length - rpos;
            }
            else {
                rsize = len_read;
            }

            memcpy(*rbuf + rpos, argsbuffer, (size_t) rsize);
            rpos += rsize;
        }
    }

    return rc;
}

/**
 * Utility to append a new caveat, freeing the old struct
 */
static const char *append_caveat(apr_pool_t *pool, struct macaroon **M, const char *format, ...)
{
    enum macaroon_returncode returncode = MACAROON_SUCCESS;
    struct macaroon *N = NULL;
    va_list args;

    va_start(args, format);
    const char *str = apr_pvsprintf(pool, format, args);
    va_end(args);

    N = macaroon_add_first_party_caveat(*M, (const unsigned char*)str, strlen(str), &returncode);
    if (!N) {
        return macaroon_error(returncode);
    }

    macaroon_destroy(*M);
    *M = N;
    return NULL;
}

/**
 * Add requested caveats to the macaroon
 * @return NULL on success, and error message otherwise
 */
static const char* append_requested_caveats(struct macaroon **M, const char *req_body)
{
    if (!req_body)
        return NULL;
    
    json_object *req = json_tokener_parse(req_body);
    if (req) {
        json_object *caveats = NULL;
        if (json_object_object_get_ex(req, "caveats", &caveats)) {
            array_list *caveats_list = json_object_get_array(caveats);
            int i = 0, arraylen = array_list_length(caveats_list);
            for (i = 0; i < arraylen; ++i) {
                json_object *value = array_list_get_idx(caveats_list, i);
                const char *value_str = json_object_get_string(value);

                enum macaroon_returncode returncode;
                struct macaroon *N;

                N = macaroon_add_first_party_caveat(*M, (const unsigned char*)value_str, strlen(value_str), &returncode);
                if (!N) {
                    return macaroon_error(returncode);
                }

                macaroon_destroy(*M);
                *M = N;
            }
        }
        json_object_put(req);
    }
    return NULL;
}

/**
 * Append default caveats
 * @return NULL on success
 */
static const char *append_default_caveats(struct macaroon **M, const dav_resource *resource)
{
    request_rec *r = resource->info->request;
    int i = 0;
    const char *err_msg = NULL;
    dmlite_security_context *sctx = NULL;
    if (resource->info->ctx)
      dmlite_get_security_context(resource->info->ctx);
    
    // If dmlite filled the security context with information about the user groups,
    // let's take that
    // Otherwise we pick info about the connected client
    if (sctx)
    {
      
      err_msg = append_caveat(r->pool, M, "dn:%s", sctx->user);
      if (err_msg) {
        return err_msg;
      }
      // Let's put all the fqans
      if (sctx->ngroups) {
        int ii;
        for (ii = 0; ii < sctx->ngroups; ii++)
          err_msg = append_caveat(r->pool, M, "fqan:%s", sctx->groups[0]);
      }
      else {
        err_msg = append_caveat(r->pool, M, "fqan:%s", "");
      }
      if (err_msg) {
        return err_msg;
      }
    }
    else {
      err_msg = append_caveat(r->pool, M, "dn:%s", resource->info->user_creds->client_name);
      if (err_msg) {
        return err_msg;
      }
      // Exactly one fqan
      if (resource->info->user_creds->nfqans) {
        err_msg = append_caveat(r->pool, M, "fqan:%s", resource->info->user_creds->fqans[i]);
      }
      else {
        err_msg = append_caveat(r->pool, M, "fqan:%s", "");
      }
      if (err_msg) {
        return err_msg;
      }
    }
    
    
    err_msg = append_caveat(r->pool, M, "path:%s", resource->info->sfn);
    if (err_msg) {
        return err_msg;
    }
    return NULL;
}

/**
 * Generate a Macaroon for the given resource. It also process the request body if necessary
 * @param resource
 * @param macaroon
 * @return
 */
static dav_error *dav_ns_generate_macaroon(const dav_resource *resource, unsigned char **macaroon)
{
    request_rec *r = resource->info->request;
    struct macaroon *M = NULL;
    enum macaroon_returncode m_ret_code = MACAROON_SUCCESS;
    dav_error *error = NULL;

    if (!resource->info->d_conf->macaroon_secret || !resource->info->d_conf->macaroon_secret_size) {
        error = dav_shared_new_error(resource->info->request, NULL, HTTP_FORBIDDEN,
            "Macaroons misconfigured");
        goto done;
    }

    // Protect from big POST, since we are going to put it into memory
    if (r->remaining > 5120) {
        error = dav_shared_new_error(resource->info->request, NULL, HTTP_BAD_REQUEST,
            "POST request too big");
        goto done;
    }

    // Read request body
    char *raw_req_body = NULL;
    apr_off_t raw_req_body_size = 0;
    if (read_body(r, &raw_req_body, &raw_req_body_size) != 0) {
        error = dav_shared_new_error(resource->info->request, NULL, HTTP_BAD_REQUEST,
            "Chunked input not supported");
        goto done;
    }

    // Prepare Macaroon
    M = macaroon_create(
        (const unsigned char*)resource->uri, strlen(resource->uri),
        (const unsigned char*)resource->info->d_conf->macaroon_secret, resource->info->d_conf->macaroon_secret_size,
        (const unsigned char*)"config", 6,
        &m_ret_code);
    if (!M) {
        error = dav_shared_new_error(r, NULL, HTTP_INTERNAL_SERVER_ERROR,
            "Could not generate the macaroon: %s", macaroon_error(m_ret_code));
        goto done;
    }

    const char *err_msg = append_default_caveats(&M, resource);
    if (err_msg) {
        error = dav_shared_new_error(r, NULL, HTTP_INTERNAL_SERVER_ERROR,
            "Failed to append default caveats: %s", err_msg);
        goto done;
    }

    err_msg = append_requested_caveats(&M, raw_req_body);
    if (err_msg) {
        error = dav_shared_new_error(r, NULL, HTTP_INTERNAL_SERVER_ERROR,
            "Failed to append the requested caveats: %s", err_msg);
        goto done;
    }

    // Serialize
    size_t msize = macaroon_serialize_size_hint(M, MACAROON_V1);
    *macaroon = apr_pcalloc(resource->pool, msize);
    macaroon_serialize(M, MACAROON_V1, *macaroon, msize, &m_ret_code);

done:
    macaroon_destroy(M);
    return error;
}


static dav_error *dav_ns_write_macaroon(const dav_resource *resource, const unsigned char *macaroon, ap_filter_t *output)
{
    dmlite_url url;
    strncpy(url.path, resource->uri, sizeof(url.path));
    strncpy(url.domain, resource->info->request->hostname, sizeof(url.domain));
    url.port = resource->info->request->connection->local_addr->port;
    url.path[sizeof(url.path)-1] = '\0';
    url.domain[sizeof(url.domain)-1] = '\0';
    url.query = NULL;

    const char *url_str = dav_shared_build_url(resource->pool, &url, &resource->info->d_conf->redirect, 0);

    apr_bucket_brigade *bb;
    apr_bucket *bkt;

    bb = apr_brigade_create(resource->pool, output->c->bucket_alloc);

    ap_fprintf(output, bb,
        "{\n"
        "    \"macaroon\": \"%s\",\n"
        "    \"uri\": {\n"
        "        \"targetWithMacaroon\": \"%s?authz=%s\",\n"
        "        \"target\": \"%s\"\n"
        "      }\n"
        "}",
        macaroon, url_str, macaroon, url_str
    );

    // Flush
    bkt = apr_bucket_eos_create(output->c->bucket_alloc);
    APR_BRIGADE_INSERT_TAIL(bb, bkt);
    if (ap_pass_brigade(output, bb) != APR_SUCCESS) {
        return dav_shared_new_error(resource->info->request, NULL,
            HTTP_INTERNAL_SERVER_ERROR, "Could not write EOS to filter.");
    }
    return NULL;
}


dav_error *dav_ns_handle_post(const dav_resource *resource, ap_filter_t *output)
{
    const char *content_type = apr_table_get(resource->info->request->headers_in, "content-type");
    if (!content_type) {
        return dav_shared_new_error(resource->info->request, NULL, HTTP_METHOD_NOT_ALLOWED, "Missing Content-Type");
    }

    if (strcasecmp(content_type, "application/macaroon-request") != 0) {
        return dav_shared_new_error(resource->info->request, NULL, HTTP_METHOD_NOT_ALLOWED, "Unsupported Content-Type");
    }

    unsigned char *macaroon = NULL;
    dav_error *err = dav_ns_generate_macaroon(resource, &macaroon);
    if (err) {
        return err;
    }
    return dav_ns_write_macaroon(resource, macaroon, output);
}


static dav_error *get_token_from_url(request_rec *r, const char **token)
{
    unsigned size;
    apr_table_t *args = dav_shared_parse_query(r->pool, r->parsed_uri.query, &size);
    const char *authz = apr_table_get(args, "authz");
    if (authz) {
        *token = apr_pstrdup(r->pool, authz);
    }
    apr_table_clear(args);
    return NULL;
}


static dav_error *get_token_from_header(request_rec *r, const char **token)
{
    const char *auth_header = apr_table_get(r->headers_in, "Authorization");
    if (!auth_header) {
        return NULL;
    }

    if (strncasecmp(auth_header, "BEARER ", 7) != 0) {
        return dav_shared_new_error(r, NULL, HTTP_FORBIDDEN, "Unsupported authorization header");
    }
    *token = auth_header + 7;
    return NULL;
}

/**
 * Called by the Macaroon verifier
 * @note: Need to make sure we use the most restrictive setting
 */
static int verify_caveat(void *data, const unsigned char *pred, size_t pred_sz)
{
    dav_resource_private *info = (dav_resource_private*)data;
    char *str = apr_pstrmemdup(info->request->pool, (const char*)pred, pred_sz);

    // User DN
    if (strncmp(str, "dn:", 3) == 0) {
      info->user_creds->client_name = str + 3;
      return 0;
    }
    // FQAN
    else if (strncmp(str, "fqan:", 5) == 0) {
      const char **pp = apr_pcalloc(info->request->pool, sizeof(const char*)*(info->user_creds->nfqans+1));
      memcpy(pp, info->user_creds->fqans,
             sizeof(const char*)*info->user_creds->nfqans);
      info->user_creds->fqans[info->user_creds->nfqans] = str + 5;
      info->user_creds->nfqans++;
      return 0;
    }
    // Path
    else if (strncmp(str, "path:", 5) == 0) {
        if (strlen(info->sfn) != pred_sz - 5) {
            return -1;
        }
        return strncmp(str + 5, info->sfn, pred_sz - 5);
    }
    // Activity
    else if (strncmp(str, "activity:", 9) == 0) {
        activity_t activities = activities_str_to_val(str + 9);
        int allowed = 0;
        switch (info->request->method_number) {
            case M_PUT:
                allowed = activities & ACTIVITY_UPLOAD;
                break;
            case M_GET:
                if (info->request->header_only) {
                    allowed = activities & ACTIVITY_LIST;
                }
                else {
                    allowed = activities & ACTIVITY_DOWNLOAD;
                }
                break;
            case M_COPY:
                if (apr_table_get(info->request->headers_in, "Destination") != NULL) {
                    allowed = activities & ACTIVITY_DOWNLOAD;
                }
                else {
                    allowed = activities & ACTIVITY_UPLOAD;
                }
                break;
            case M_PROPFIND:
                allowed = activities & ACTIVITY_LIST;
                break;
            case M_DELETE:
                allowed = activities & ACTIVITY_DELETE;
                break;
            case M_PROPPATCH:
                allowed = activities & ACTIVITY_MANAGE;
                break;
        }
        // Expect 0 as true
        return !allowed;
    }
    // Before
    else if (strncmp(str, "before:", 7) == 0) {
        struct tm tp = {0};
        if (strptime(str + 7, "%FT%T%Z", &tp) == NULL) {
            return -1;
        }

        apr_time_exp_t time_exp = {
            0, tp.tm_sec, tp.tm_min, tp.tm_hour, tp.tm_mday,
            tp.tm_mon, tp.tm_year, tp.tm_wday, tp.tm_yday,
            tp.tm_isdst, tp.tm_gmtoff
        };

        apr_time_t before;
        apr_time_exp_get(&before, &time_exp);

        if (before < apr_time_now()) {
            return -1;
        }
        return 0;
    }

    // Unknown!
    return -1;
}

/**
 * Deserialize and verify the token, and initialize the relevant fields
 */
static dav_error *dav_ns_deserialize_bearer(dav_resource_private *info, const char *token)
{
    struct macaroon *M = NULL;
    struct macaroon_verifier *V = NULL;
    enum macaroon_returncode returncode = MACAROON_SUCCESS;
    dav_error *error = NULL;
    int rc = 0;
    dmlite_credentials *prevcreds = info->user_creds;
    
    info->user_creds =  apr_pcalloc(info->request->pool, sizeof(dmlite_credentials));
    info->user_creds->fqans = apr_pcalloc(info->request->pool, sizeof(char*) * DAV_SHARED_MAX_FQANS);
    info->user_creds->nfqans = 0;
    
    V = macaroon_verifier_create();
    rc = macaroon_verifier_satisfy_general(V, verify_caveat, info, &returncode);
    if (rc != 0) {
        error = dav_shared_new_error(info->request, NULL, HTTP_BAD_REQUEST,
            "Could not initialize the verifier: %s", macaroon_error(returncode));
        goto done;
    }

    M = macaroon_deserialize((const unsigned char*)token, strlen(token), &returncode);
    if (!M) {
        error = dav_shared_new_error(info->request, NULL, HTTP_BAD_REQUEST,
            "Could not deserialize the token: %s", macaroon_error(returncode));
        goto done;
    }

    
    const char *precmech = info->user_creds->mech;
    info->user_creds->mech = MACAROON_MECH;
#if AP_SERVER_MAJORVERSION_NUMBER == 2 &&\
    AP_SERVER_MINORVERSION_NUMBER < 4
    info->user_creds->remote_address = info->request->connection->remote_ip;
#else
    info->user_creds->remote_address = info->request->useragent_ip;
#endif

    
    rc = macaroon_verify(V, M,
        (const unsigned char*)info->d_conf->macaroon_secret, info->d_conf->macaroon_secret_size,
        NULL, 0,
        &returncode);

    if (rc != 0) {
        error = dav_shared_new_error(info->request, NULL, HTTP_FORBIDDEN,
            "Could not verify the token: %s", macaroon_error(returncode));
        
        // Wipe the credentials then, undo the changes
        info->user_creds = prevcreds;
        goto done;
    }

    
    info->user_creds->extra = dmlite_any_dict_new();
    
done:
    macaroon_verifier_destroy(V);
    macaroon_destroy(M);
    return error;
}


dav_error *dav_ns_init_user_from_macaroon(dav_resource_private *info)
{
    const char *token = NULL;
    dav_error *error = NULL;
    request_rec *r = info->request;

    error = get_token_from_url(r, &token);
    if (!error && !token) {
        error = get_token_from_header(r, &token);
    }

    if (error) {
        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, info->request,
            "Header not accepted for Macaroons: %s", error->desc);
        return NULL;
    }
    else if (!token) {
        return NULL;
    }

    ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, r, "Got bearer token %s", token);

    //info->user_creds = apr_pcalloc(r->pool, sizeof(dmlite_credentials));
    //info->user_creds->mech = MACAROON_MECH;
    
    error = dav_ns_deserialize_bearer(info, token);
    if (error) {
        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, info->request,
            "Bearer token not accepted for Macaroons: %s", error->desc);
    }
    return NULL;
}
