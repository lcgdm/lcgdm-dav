/*
 * Copyright (c) CERN 2017
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MOD_LCGDM_NS_MACAROONS_H
#define MOD_LCGDM_NS_MACAROONS_H

typedef enum {
    ACTIVITY_NONE = 0,
    ACTIVITY_DOWNLOAD = 1 << 0,
    ACTIVITY_UPLOAD   = 1 << 1,
    ACTIVITY_LIST     = 1 << 2,
    ACTIVITY_DELETE   = 1 << 3,
    ACTIVITY_MANAGE   = 1 << 4,
} activity_t;

#define MACAROON_MECH "macaroon"

/**
 * Prepare the private info from a bearer token
 */
dav_error *dav_ns_init_user_from_macaroon(dav_resource_private *info);

#endif // MOD_LCGDM_NS_MACAROONS_H
