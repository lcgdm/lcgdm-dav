/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <apr_strings.h>
#include <ctype.h>
#include <json.h>
#include "replicas.h"
#include "../shared/utils.h"

char* dav_ns_serialize_replicas(request_rec *req, int nreplicas,
        dmlite_replica *replicas)
{
    int i;
    char *serial;
    char buffer[1024];

    apr_pool_t *pool = req->pool;

    serial = "[\n";

    for (i = 0; i < nreplicas; ++i) {
        serial = apr_psprintf(pool, "%s"
                "{\n"
                "\t\"server\"    : \"%s\",\n"
                "\t\"rfn\"       : \"%s\",\n"
                "\t\"atime\"     : %lu,\n"
                "\t\"status\"    : \"%c\",\n"
                "\t\"type\"      : \"%c\",\n"
                "\t\"ltime\"     : %lu", serial, replicas[i].server,
                replicas[i].rfn, replicas[i].atime,
                replicas[i].status ? replicas[i].status : '?',
                replicas[i].type ? replicas[i].type : '?', replicas[i].ltime);

        if (replicas[i].extra != NULL ) {
            serial = apr_psprintf(pool, "%s,\n"
                    "\t\"extra\": %s\n", serial,
                    dmlite_any_dict_to_json(replicas[i].extra, buffer,
                            sizeof(buffer)));
        }

        if (i + 1 < nreplicas)
            serial = apr_pstrcat(pool, serial, "},\n", NULL );
        else
            serial = apr_pstrcat(pool, serial, "}\n", NULL );
    }

    serial = apr_pstrcat(pool, serial, "]", NULL );

    return serial;
}


dav_error* dav_ns_deserialize_replicas(request_rec *req, const char* json_str,
        struct dav_ns_replica_array* replicas)
{
    struct json_object *json = json_tokener_parse(json_str);
    int i;
    apr_pool_t *pool = req->pool;

    /* Just in case */
    if (json == NULL )
        return dav_shared_new_error(req, NULL, HTTP_CONFLICT,
                "Could not parse the JSON string");

    /* Weirdness in the error handling of json_tokener_parser :/  */
    enum json_tokener_error error =
            (enum json_tokener_error) (-(ptrdiff_t) json);
    if (error > json_tokener_success
            && error <= json_tokener_error_parse_comment)
        return dav_shared_new_error(req, NULL, HTTP_CONFLICT,
                "Could not parse the JSON string");

    /* Must be an array! */
    if (json_object_get_type(json) != json_type_array)
        return dav_shared_new_error(req, NULL, HTTP_CONFLICT,
                "First-level JSON Object must be an array");

    /* Initialize the array */
    replicas->nreplicas = json_object_array_length(json);
    replicas->replicas =
            apr_pcalloc(pool, sizeof(dmlite_replica) * replicas->nreplicas);
    replicas->action = apr_pcalloc(pool, sizeof(char) * replicas->nreplicas);

    /* Iterate and copy */
    for (i = 0; i < replicas->nreplicas; ++i) {
        struct json_object *json_replica = json_object_array_get_idx(json, i);

        /* Get the json objects */
        struct json_object *server = NULL;
        json_object_object_get_ex(json_replica, "server", &server);

        struct json_object *rfn = NULL;
        json_object_object_get_ex(json_replica, "rfn", &rfn);

        struct json_object *status = NULL;
        json_object_object_get_ex(json_replica, "status", &status);

        struct json_object *type = NULL;
        json_object_object_get_ex(json_replica, "type", &type);

        struct json_object *action = NULL;
        json_object_object_get_ex(json_replica, "action", &action);

        struct json_object *ltime = NULL;
        json_object_object_get_ex(json_replica, "ltime", &ltime);

        struct json_object *atime = NULL;
        json_object_object_get_ex(json_replica, "atime", &atime);

        /* Get the actual values */
        if (server != NULL
                && json_object_get_type(server) == json_type_string) {
            strncpy(replicas->replicas[i].server,
                    json_object_get_string(server), HOST_NAME_MAX);
            replicas->replicas[i].server[HOST_NAME_MAX - 1] = '\0';
        }
        if (rfn != NULL && json_object_get_type(rfn) == json_type_string) {
            strncpy(replicas->replicas[i].rfn, json_object_get_string(rfn),
                    URL_MAX);
            replicas->replicas[i].rfn[URL_MAX - 1] = '\0';
        }
        if (status != NULL && json_object_get_type(status) == json_type_string)
            replicas->replicas[i].status = json_object_get_string(status)[0];
        if (type != NULL && json_object_get_type(type) == json_type_string)
            replicas->replicas[i].type = json_object_get_string(type)[0];
        if (ltime != NULL && json_object_get_type(ltime) == json_type_int)
            replicas->replicas[i].ltime = json_object_get_int(ltime);
        if (atime != NULL && json_object_get_type(atime) == json_type_boolean)
            replicas->replicas[i].atime = json_object_get_boolean(atime);

        if (action != NULL && json_object_get_type(action) == json_type_string)
            replicas->action[i] = toupper(json_object_get_string(action)[0]);
        else
            replicas->action[i] = 'M';

        /* Pass the 'extra' */
        struct json_object *extra = NULL;
        json_object_object_get_ex(json_replica, "extra", &extra);

        if (extra != NULL ) {
            replicas->replicas[0].extra = dmlite_any_dict_from_json(
                    json_object_get_string(extra));
            if (replicas->replicas[0].extra == NULL )
                return dav_shared_new_error(req, NULL, HTTP_CONFLICT,
                        "Could not parse the JSON string");
        }
        else {
            replicas->replicas[0].extra = NULL;
        }
    }

    /* Weird name for a free function... */
    json_object_put(json);

    return NULL ;
}
