/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <apr_strings.h>
#include "mod_lcgdm_post.h"

struct filter_context
{
    size_t boundary_len, buflen;
    const char *boundary;
    char *buffer;
    enum
    {
        kReading, kCandidate, kDone
    } stage;
};
typedef struct filter_context filter_context;

void* form_filter_new_context(request_rec *r, const char* boundary)
{
    filter_context* ctx = apr_pcalloc(r->pool, sizeof(*ctx));
    ctx->boundary = apr_pstrcat(r->pool, "\r\n--", boundary, NULL );
    ctx->boundary_len = strlen(ctx->boundary);
    ctx->buffer = apr_pcalloc(r->pool, ctx->boundary_len);
    ctx->buflen = 0;

    ctx->stage = kReading;

    return ctx;
}

static int fetch_remaining_boundary(filter_context *ctx, const char *data,
        apr_size_t datalen)
{
    apr_size_t remaining = ctx->boundary_len - ctx->buflen;
    if (datalen < remaining) {
        memcpy(ctx->buffer + ctx->buflen, data, datalen);
        ctx->buflen += datalen;
        ctx->stage = kCandidate;
    }
    else {
        memcpy(ctx->buffer + ctx->buflen, data, remaining);
        // Match! Finish this
        if (strncmp(ctx->buffer, ctx->boundary, ctx->boundary_len) == 0)
            ctx->stage = kDone;
        else
            ctx->stage = kReading;
    }
    return ctx->stage;
}

static int search_boundary(filter_context *ctx, const char *data,
        apr_size_t *datalen)
{
    const char *p = data;
    apr_size_t remaining;
    const char *end_data = data + *datalen;

    while (ctx->stage == kReading
            && (p = memchr(p, '\r', end_data - p)) != NULL ) {
        remaining = end_data - p;

        // Do we have enough to read the whole candidate?
        if (remaining >= ctx->boundary_len) {
            memcpy(ctx->buffer, p, ctx->boundary_len);
            // We have a match, so we are done here
            if (strncmp(ctx->buffer, ctx->boundary, ctx->boundary_len) == 0) {
                *datalen -= remaining;
                ctx->stage = kDone;
            }
        }
        // No, we don't, so copy what's left to the buffer,
        // return only the left size of the data, and change stage
        else {
            memcpy(ctx->buffer, p, remaining);
            ctx->buflen = remaining;
            ctx->stage = kCandidate;
            *datalen -= remaining;
        }
        p += 1;
    }
    return ctx->stage;
}

static int process_bucket(filter_context *ctx, ap_filter_t *f, apr_bucket *b,
        apr_bucket_brigade *out)
{
    const char *data;
    apr_size_t len;

    int ret = apr_bucket_read(b, &data, &len, APR_BLOCK_READ);
    if (ret != APR_SUCCESS)
        return ret;

    switch (ctx->stage) {
        // We didn't fill the buffer, so read until it is full
        case kCandidate:
            if (fetch_remaining_boundary(ctx, data, len) != kReading)
                break;
            // Didn't match, and there is data in the buffer, so pass it
            apr_brigade_write(out, ap_filter_flush, f, ctx->buffer,
                    ctx->buflen);
            ctx->stage = kReading;
            ctx->buflen = 0;
            /* no break */
            // Search for \r
        case kReading:
            search_boundary(ctx, data, &len);
            if (len > 0)
                apr_brigade_write(out, ap_filter_flush, f, data, len);
            if (ctx->stage == kDone)
                APR_BRIGADE_INSERT_TAIL(out,
                        apr_bucket_eos_create(out->bucket_alloc));
            break;
            // Just keep going, and send nothing back,
            // until we exhaust the input
        case kDone:
            break;
    }

    return APR_SUCCESS;
}

apr_status_t form_filter(ap_filter_t *f, apr_bucket_brigade *out,
        ap_input_mode_t mode, apr_read_type_e block, apr_off_t readbytes)
{
    filter_context* ctx = f->ctx;

    // Note: since we are PUT'ting a file, we don't really care
    //       much about reading into the next field, since we
    //       are going to trash it anyway.

    apr_bucket_brigade* in;
    in = apr_brigade_create(f->r->pool, f->r->connection->bucket_alloc);

    // Read fron next filter
    int ret = ap_get_brigade(f->next, in, mode, block, readbytes);
    if (ret != APR_SUCCESS)
        return ret;

    apr_bucket* b = NULL;
    for (b = APR_BRIGADE_FIRST(in); b != APR_BRIGADE_SENTINEL(in) ; b =
            APR_BUCKET_NEXT(b)) {

        if (APR_BUCKET_IS_EOS(b)) {
            APR_BRIGADE_INSERT_TAIL(out,
                    apr_bucket_eos_create(out->bucket_alloc));
            break;
        }
        else if (APR_BUCKET_IS_METADATA(b)) {
            continue;
        }
        else {
            ret = process_bucket(ctx, f, b, out);
            if (ret != APR_SUCCESS)
                break;
        }
    }

    apr_brigade_destroy(in);
    return ret;
}
