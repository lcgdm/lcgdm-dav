/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef MOD_LCGDM_POST_H_
#define MOD_LCGDM_POST_H_

#include <httpd.h>
#include <http_config.h>
#include <util_filter.h>

#define POST_HANDLER "post2put-handler"

/** Per-directory configuration */
struct post_dir_conf
{
    const char *field;
};
typedef struct post_dir_conf post_dir_conf;

extern module lcgdm_post_module;

/** Handle the request */
int post_handler(request_rec *r);

/** Create a new form filter context */
void* form_filter_new_context(request_rec *r, const char* boundary);

/** Form filter function */
apr_status_t form_filter(ap_filter_t *f, apr_bucket_brigade *out,
        ap_input_mode_t mode, apr_read_type_e block, apr_off_t readbytes);

#endif
