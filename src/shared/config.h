/* 
 * File:   config.h.cmake
 * Author: Alejandro Álvarez Ayllón
 */
#ifndef CONFIG_H_
#define	CONFIG_H_


/* Version */
#define LCGDM_DAV_MAJOR 0
#define LCGDM_DAV_MINOR 23
#define LCGDM_DAV_PATCH 0

#define LCGDM_DAV_VERSION "0.23.0"

/* Flags */
#define WITH_METALINK
/* #undef USE_GNUTLS */

#endif	/* CONFIG_H_ */
