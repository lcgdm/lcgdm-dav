/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <apr_strings.h>
#include <apr_xml.h>
#include <ctype.h>
#include <dmlite/c/dmlite.h>
#include <dmlite/c/io.h>
#include <httpd.h>

#include "config.h"
#include "utils.h"
#include "use_module.h"

dav_error* dav_shared_new_error(request_rec *r, dmlite_context *ctx,
        int http_code, const char *format, ...)
{
    const char *ctx_msg = "";
    const char *msg = "";

    if (ctx != NULL) {
        ctx_msg = dmlite_error(ctx);

        /* Map err_no to HTTP status code, if needed */
        if (http_code == 0) {
            int err_code = dmlite_errno(ctx);
            switch (err_code) {
                case EINPROGRESS:
                    http_code = HTTP_ACCEPTED;
                    break;
                case ENOSYS:
                    http_code = HTTP_NOT_IMPLEMENTED;
                    break;

                case DMLITE_NO_REPLICAS:
                case ENOENT:
                    http_code = HTTP_NOT_FOUND;
                    break;

                case EEXIST:
                case EISDIR:
                    http_code = HTTP_CONFLICT;
                    break;

                case ENOTDIR:
                    http_code = HTTP_NOT_FOUND;
                    break;

                case EACCES:
                case DMLITE_NO_SUCH_USER:
                case DMLITE_NO_SUCH_GROUP:
                case DMLITE_NO_USER_MAPPING:
                    http_code = HTTP_FORBIDDEN;
                    break;

                case ENOSPC:
                    http_code = HTTP_INSUFFICIENT_STORAGE;
                    break;

#ifdef ECOMM
                case ECOMM:
#endif
                case EBUSY:
                    http_code = HTTP_SERVICE_UNAVAILABLE;
                    break;

                default:
                    http_code = HTTP_INTERNAL_SERVER_ERROR;
            }
        }
    }

    /* If extra information has been specified, generate the string */
    if (format) {
        va_list extra;

        va_start(extra, format);
        msg = apr_pvsprintf(r->pool, format, extra);
        va_end(extra);
    }

    msg = apr_xml_quote_string(r->pool,
            apr_psprintf(r->pool, "%s (%s)", msg, ctx_msg), 1);

    /* Set error note */
    apr_table_setn(r->notes, "error-notes", msg);
    apr_table_setn(r->subprocess_env, "LCGDM_DAV_VERSION", LCGDM_DAV_VERSION);

#if AP_SERVER_MAJORVERSION_NUMBER == 2 &&\
    AP_SERVER_MINORVERSION_NUMBER <= 4
    return dav_new_error(r->pool, http_code, 0, msg);
#else
    return dav_new_error(r->pool, http_code, 0,
            (http_code < 400) ? APR_SUCCESS : APR_EGENERAL,
            msg);
#endif
}

int dav_shared_format_datetime(char *buffer, size_t maxlen, time_t tstamp,
        unsigned format)
{
    struct tm tms;
    int l;

    gmtime_r(&tstamp, &tms);

    switch (format) {
        case DAV_DPM_RFC2068:
            l = snprintf(buffer, maxlen, "%s, %.2d %s %d %.2d:%.2d:%.2d GMT",
                    apr_day_snames[tms.tm_wday], tms.tm_mday,
                    apr_month_snames[tms.tm_mon], tms.tm_year + 1900,
                    tms.tm_hour, tms.tm_min, tms.tm_sec);
            break;
        case DAV_DPM_RFC3339:
        default:
            l = snprintf(buffer, maxlen, "%d-%.2d-%.2dT%.2d:%.2d:%.2dZ",
                    tms.tm_year + 1900, tms.tm_mon + 1, tms.tm_mday,
                    tms.tm_hour, tms.tm_min, tms.tm_sec);
    }

    return l;
}

char *dav_shared_mode_str(char *str, mode_t mode)
{
    str[10] = '\0';

    if (S_ISDIR(mode))
        str[0] = 'd';
    else if (S_ISLNK(mode))
        str[0] = 'l';
    else
        str[0] = '-';

    str[1] = (mode & S_IRUSR) ? 'r' : '-';
    str[2] = (mode & S_IWUSR) ? 'w' : '-';
    if (mode & S_ISUID)
        str[3] = (mode & S_IXUSR) ? 's' : 'S';
    else
        str[3] = (mode & S_IXUSR) ? 'x' : '-';

    str[4] = (mode & S_IRGRP) ? 'r' : '-';
    str[5] = (mode & S_IWGRP) ? 'w' : '-';
    if (mode & S_ISGID)
        str[6] = (mode & S_IXGRP) ? 's' : 'S';
    else
        str[6] = (mode & S_IXGRP) ? 'x' : '-';

    str[7] = (mode & S_IROTH) ? 'r' : '-';
    str[8] = (mode & S_IWOTH) ? 'w' : '-';
    if (mode & S_ISVTX)
        str[9] = (mode & S_IXOTH) ? 't' : 'T';
    else
        str[9] = (mode & S_IXOTH) ? 'x' : '-';

    return str;
}

char *dav_shared_size_str(char *str, size_t max, off_t size)
{
    static const off_t TERABYTE = 1099511627776;
    static const off_t GIGABYTE = 1073741824;
    static const off_t MEGABYTE = 1048576;
    static const off_t KILOBYTE = 1024;

    if (size >= TERABYTE)
        snprintf(str, max, "%.1fT", (float) size / 1099511626752.0);
    else if (size >= GIGABYTE)
        snprintf(str, max, "%.1fG", (float) size / 1073741824.0);
    else if (size >= MEGABYTE)
        snprintf(str, max, "%.1fM", (float) size / 1048576.0);
    else if (size >= KILOBYTE)
        snprintf(str, max, "%.1fK", (float) size / 1024.0);
    else
        snprintf(str, max, "%zu", size);
    return str;
}

void dav_shared_add_response(dav_walk_resource *wres, dav_error *error)
{
    dav_response *resp;

    resp = apr_pcalloc(wres->pool, sizeof(dav_response));
    resp->href = apr_pstrdup(wres->pool, wres->resource->uri);
    resp->status = error->status;
    resp->desc = error->desc;

    resp->next = wres->response;
    wres->response = resp;
}

apr_table_t *dav_shared_parse_query(apr_pool_t *pool, const char *query,
        unsigned *size)
{
    apr_table_t *table;
    char *copy, *last, *t, *p;

    table = apr_table_make(pool, 0);
    *size = 0;

    if (!query)
        return table;

    copy = apr_pstrdup(pool, query);

    /* Split by & (fields) */
    t = apr_strtok(copy, "&", &last);
    while (t) {
        /* Split by = (values) */
        p = strchr(t, '=');
        if (p == NULL ) {
            p = "";
        }
        else {
            *p = '\0';
            ++p;
        }
        /* Now, in t we have the field, and in p the value*/
        apr_table_set(table, t, p);
        ++(*size);
        /* Next */
        t = apr_strtok(NULL, "&", &last);
    }

    return table;
}

char *dav_shared_build_url(apr_pool_t *pool, dmlite_url *url,
        const struct redirect_cfg* redirect, char force_secure)
{
    char buffer[1024];

    /* Change the scheme if it doesn't fit us */
    if (strncmp(url->scheme, "http", 4) != 0 || force_secure) {

        char use_secure = force_secure
                || (strcmp(redirect->scheme, "https") == 0);

        if (use_secure) {
            strncpy(url->scheme, "https", sizeof(url->scheme));
            url->port = redirect->port_secure;
        }
        else {
            strncpy(url->scheme, "http", sizeof(url->scheme));
            url->port = redirect->port_unsecure;
        }
    }

    /* dmlite_url_serialize does *not* escape special characters in the path (i.e. #)
     * Indeed, given its name if probably ought to do it, but there is other code (dpm-xrootd)
     * relying on it not to (see LCGDM-2013)
     * Also, DB historically does *not* store the replicas escaped.
     */
    strncpy(url->path, ap_os_escape_path(pool, url->path, 1), sizeof(url->path) - 1);

    /* This serializes the URL as we want */
    dmlite_url_serialize(url, buffer, sizeof(buffer));

    return apr_pstrdup(pool, buffer);
}

int dav_shared_request_accepts(request_rec* r, const char* content)
{
    const char* p = apr_table_get(r->headers_in, "Accept");

    while (p) {
        while (isspace(*p) || ispunct(*p))
            ++p;
        if (strncmp(content, p, 25) == 0)
            return 1;
        p = strchr(p, ',');
    }

    return 0;
}

apr_status_t dav_shared_context_free(void* context)
{
    dmlite_context_free((dmlite_context*) context);
    return APR_SUCCESS;
}

apr_status_t dav_shared_dict_free(void* dict)
{
    dmlite_any_dict_free((dmlite_any_dict*) dict);
    return APR_SUCCESS;
}

apr_status_t dav_shared_fclose(void* fd)
{
    dmlite_fclose((dmlite_fd*) fd);
    return APR_SUCCESS;
}


void dav_lcgdm_notify_support_external_copy(request_rec *req)
{
    apr_table_set(req->notes, LCGDM_DAV_NOTE_COPY_KEY, LCGDM_DAV_NOTE_COPY_EXTERNAL);
}


apr_table_t* dav_lcgdm_parse_cookies(apr_pool_t* pool, const char* cookie_str)
{
    apr_table_t* cookies = apr_table_make(pool, 10);
    const char* pair;
    while (*cookie_str && (pair = ap_getword(pool, &cookie_str, ';')) != NULL) {
        while (*cookie_str == ' ') ++cookie_str;

        char *name, *value;
        name = ap_getword(pool, &pair, '=');
        value = (char*)pair;
        ap_unescape_url(value);

        apr_table_set(cookies, name, value);
    }
    return cookies;
}
