cmake_minimum_required (VERSION 2.6)

install (DIRECTORY   ${CMAKE_CURRENT_SOURCE_DIR}/
         DESTINATION usr/share/lcgdm-dav
         FILES_MATCHING
         PATTERN ".*" EXCLUDE
         PATTERN "CMakeFiles" EXCLUDE
         PATTERN "DPMbox/thesis" EXCLUDE
         PATTERN "robots.txt"
         PATTERN "*.png"
         PATTERN "*.css"
         PATTERN "*.js"
         PATTERN "*.shtml"
         PATTERN "*.html"
)
